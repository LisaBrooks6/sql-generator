public class Check {
	private String checkOperator;
	private String value;
	
	/**
	 * Constructor for Check
	 * @param op - String containing the operator
	 * @param val - String containing the value
	 */
	public Check(String op, String val){
		this.checkOperator = op;
		this.value = val;
	}
	
	/**
	 * Accessor for checkOperator
	 * @return checkOperator
	 */
	public String getCheckOperator() {
		return this.checkOperator;
	}
	
	/**
	 * Mutator for checkOperator 
	 * @param op 
	 */
	public void setCheckOperator(String op){
		this.checkOperator = op;
	}
	
	/**
	 * Accessor for value
	 * @return value 
	 */
	public String getValue() {
		return this.value;
	}
	
	/**
	 * Mutator for value
	 * @param val
	 */
	public void setValue(String val) {
		this.value = val;
	}
}
