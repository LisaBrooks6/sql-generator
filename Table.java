import java.util.ArrayList;

public class Table {

	private String name;
	private int noCols;
	private ArrayList<Column> columns;

	/**
	 * Constructor for Table
	 * @param name String determining the table name
	 * @param noCols int determining the number of columns in the table
	 */
	public Table(String name, int noCols){
		this.name = name;
		this.noCols = noCols;
		columns = new ArrayList<Column>();
	}

	/**
	 * Accessor to return name
	 * @return name - the String holding the table name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Accessor to return noCols 
	 * @return noCols - the int holding the number of columns in the table
	 */
	public int getNoCols() {
		return noCols;
	}

	/**
	 * Accessor to return columns
	 * @return columns - the ArrayList of Column instances related to the table
	 */
	public ArrayList<Column> getColumns() {
		return columns;
	}

	/**
	 * Mutator to set the name of the table
	 * @param name String holding the name of the table
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Mutator to set the number of columns
	 * @param cols int holding the number of columns
	 */
	public void setNoCols(int cols) {
		this.noCols = cols;
	}

	/**
	 * Mutator for columns
	 * @param columns An ArrayList holding a list of Column instances
	 */
	public void setColumns(ArrayList<Column> columns) {
		this.columns = columns;
	}

	/**
	 * Method to return the primary key of the table
	 * @return Column - the column which is the primary key of the table or null 
	 * if it doesn't exist.
	 */
	public ArrayList<String> getPrimaryKey() {
		ArrayList<String> pkNames = new ArrayList<String>();
		for(Column c : columns) {
			if(c.isPrimaryKey()){
				pkNames.add(c.getName());
			}
		}
		return pkNames;
	}

	/** 
	 * This method returns a specific Column instance based on
	 * a String holding the column name.
	 * @param colName String holding the name of the column
	 * @return Column - the corresponding column or null if the column doesn't exist
	 */
	public Column getColumn(String colName) {
		// loop through the columns in the table 
		for(int i = 0; i < columns.size(); i++){
			if(columns.get(i).getName().equals(colName)){
				return columns.get(i);
			}
		}
		return null;
	}

	/**
	 * This method attempts to add a column to the table, if the column already
	 * exists the method returns false and the column is not added again.
	 * @param colName String holding the column name
	 * @param colType Object holding the type of the column
	 * @param size Integer holding the size of the column (dependent on the type this could be null)
	 * @return String - this method will return "added" if a new column has been added or "updated" if 
	 * the column already exists and the Column mutators were used to update the type and size.
	 */
	public String addColumn(String colName, Object colType, Integer size) {
		if(!columnExistance(colName)){
			// column does not already exist, therefore can be added
			columns.add(new Column(colName, colType, size));
			return "added";
		}
		else{
			Column c = this.getColumn(colName);
			// update with the colType and size provided
			c.setType(colType);
			c.setSize(size);
			return "updated";
		}
	}

	/**
	 * This method adds constraints to the specified column by accessing the Column mutators.
	 * @param colName the specified column name (String) to be edited
	 * @param notNull the boolean will determine whether or not the column is not null
	 * @param unique boolean will determine whether or not the column is unique
	 * @param check boolean will determine whether or not the column contains any checks
	 * @param checkOps ArrayList of Strings holding the check operations - position in the arraylist
	 * corresponds to the position of the value in the checkVals arrayList
	 * @param checkVals ArrayList of Strings holding the check values - position in the arraylist will 
	 * correspond to its relative operator in the checkOps arrayList.
	 * @param defaultCheck boolean will determine whether or not the column has a default
	 * @return true - true will be returned as the mutators will have been entered
	 */
	public boolean addConstraintsToColumn(String colName, boolean notNull, boolean unique, boolean check, 
			ArrayList<Check> checks, boolean defaultCheck, String defaultVal){
		// the column already exists so get the Column
		Column c = this.getColumn(colName);

		c.setNotNull(notNull);
		c.setUnique(unique);
		c.setCheck(check);
		c.setCheckList(checks);
		c.setDefaultCheck(defaultCheck);
		c.setDefaultVal(defaultVal);

		// constraints have been added
		c.setConstraintsAdded(true);

		return true;
	}

	/**
	 * This method checks if the column is already in the array. If so it 
	 * returns true, if not it returns false and the column can be added.
	 * @param colName String holding the column name
	 * @return true/false - boolean determining the outcome
	 */
	public boolean columnExistance(String colName) {
		for(int i = 0; i < columns.size(); i++){
			if(columns.get(i) != null){
				if(columns.get(i).getName().equals(colName)){
					return true;
				}
			}
		}
		return false;
	}
}
