import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddTableInfo extends GUI implements ActionListener {
	private JButton back, fkConfig, genScripts;
	private int noTables;
	private JPanel tableInfoPanel, buttonPanel;
	private ArrayList<AddColumnInfo> addColInfo;
	private ArrayList<JLabel> nameLabel, noColLabel;
	private ArrayList<JTextField> names;
	private ArrayList<JComboBox> cols;
	private ArrayList<JButton> addColButton;

	private InitialScreen init;
	private FKConfiguration fKConfiguration = null;

	/**
	 * Constructor for AddTableInfo
	 * @param db - instance of Database 
	 * @param init - instance of InitialScreen
	 */
	public AddTableInfo(Database db, InitialScreen init) {
		this.db = db;
		this.setTitle("Schema Generation: Add Table Information");

		int noTables = db.getNoTables();
		if(noTables == 1){
			this.setSize(900,100);
		}
		else{
			this.setSize(900,65*noTables);
		}	
		this.init = init;
		this.initialSetUp();
	}


	@Override
	public void initialSetUp() {
		// initiate JComponents 
		noTables = db.getNoTables();
		addColInfo = new ArrayList<AddColumnInfo>();

		nameLabel = new ArrayList<JLabel>();
		noColLabel = new ArrayList<JLabel>();

		back = new JButton("Back");
		fkConfig = new JButton("Add Foreign Keys");
		genScripts = new JButton("Generate Scripts");

		names = new ArrayList<JTextField>();

		cols = new ArrayList<JComboBox>();
		addColButton = new ArrayList<JButton>();

		// add action listeners
		back.addActionListener(this);
		fkConfig.addActionListener(this);
		genScripts.addActionListener(this);

		// Panels and GUI set up
		tableInfoPanel = new JPanel();
		tableInfoPanel.setLayout(new GridLayout(noTables, 5));
		buttonPanel = new JPanel();

		addComponents(0, noTables);

		buttonPanel.add(back);
		if(this.addButtons()) {
			buttonPanel.add(fkConfig);
			buttonPanel.add(genScripts);
		}

		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * This method adds JComponents to the array lists of JComponents from index from to to.
	 * @param from - int holding the lower bound
	 * @param to - int holding the upper bound
	 */
	public void addComponents(int from, int to) {
		for(int i = from; i < to; i++) {
			nameLabel.add(i, new JLabel("Table Name: "));
			tableInfoPanel.add(nameLabel.get(i));

			names.add(i, new JTextField(30));
			tableInfoPanel.add(names.get(i));

			noColLabel.add(i, new JLabel("No. of columns:"));
			tableInfoPanel.add(noColLabel.get(i));

			cols.add(i, new JComboBox());
			this.populateComboBox(cols.get(i));
			tableInfoPanel.add(cols.get(i));

			addColButton.add(i, new JButton("Add Column Info"));
			addColButton.get(i).addActionListener(this);
			tableInfoPanel.add(addColButton.get(i));

			// initialise the addColInfo index to null
			addColInfo.add(i, null);
		}
		// add the tableInfoPanel to the JFrame again
		this.add(tableInfoPanel, BorderLayout.CENTER);
	}

	/**
	 * This method determines whether all column info has been entered and if so returns true for the key 
	 * configuration button to be made visible, else false is returned.
	 * @return true/false
	 */
	public boolean addButtons() {
		boolean [] allAdded = new boolean[addColButton.size()];
		for(int i = 0; i < addColButton.size(); i++){
			if(addColButton.get(i).getText().startsWith("Edit")){
				allAdded[i] = true;
			}
			else{
				allAdded[i] = false;
			}
		}

		for(boolean b : allAdded) {
			if(!b){
				// if there is one false in allAdded then the button should not be added
				return false;
			}
		}
		/* if all buttons are 'Edit col info' then all col info has been entered 
		 * and the fkConfig and generateScripts buttons can be displayed
		 */
		return true;
	}

	/**
	 * This method validates the user input and if no instance of this table exists then it is created and 
	 * a new instance of AddColumnInfo is created to add the column information to the new Table instance.
	 * Else if the table already exists then the table number of columns is updated and the correpoding 
	 * AddColumnInfo instance is made visible.
	 * @param index - the index of the row we are working on
	 */
	public void addColumnInfo(int index) {
		// get the entered information from names and cols using index
		String name = names.get(index).getText().trim();
		int noCols = 0;

		if(cols.get(index).getSelectedIndex() < 0 || name.isEmpty()) {
			// either the name is empty or no. of cols have not been selected
			if(name.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Please enter the name of the table", "Error", JOptionPane.ERROR_MESSAGE);
			}
			if(cols.get(index).getSelectedIndex() < 0) {
				JOptionPane.showMessageDialog(null, "Please choose number of columns", "Error message", JOptionPane.ERROR_MESSAGE);
			}
		}
		else{
			// both all good therefore add the table to the database
			noCols = (int) cols.get(index).getSelectedItem();

			Integer tableIndex = db.getTableIndex(name);

			if(tableIndex != null){
				// the table exists in db.tables
				if(addColButton.get(tableIndex).getText().startsWith("Edit")){
					// the column has been edited
					if(noCols < db.getTable(name).getNoCols()) {
						/* if the new number of columns is less than the current then this can't happen.
						 * We need to create a new instance of this and remove the old instance.
						 */
						db.removeTable(tableIndex);
						// add the column again
						db.addTable(name, noCols);
						// create an instance of AddColumnInfo
						AddColumnInfo cInfo = new AddColumnInfo(name, noCols, this, index, db);
						// make it visible
						cInfo.setVisible(true);	
					}
					else if(noCols == db.getTable(name).getNoCols()) {
						addColInfo.get(tableIndex).setVisible(true);
					}
					else{
						// set the specific instance of AddColumnInfo visible
						addColInfo.get(tableIndex).updateColInfo(noCols, db);
						addColInfo.get(tableIndex).setVisible(true);
					}
				}
				else{
					AddColumnInfo cInfo = new AddColumnInfo(name, noCols, this, index, db);
					// make it visible
					cInfo.setVisible(true);	
				}
			}
			else{
				db.addTable(name, noCols);
				// disable the name of table field once the table has been added
				names.get(index).setEnabled(false);
				// create an instance of the new screen 
				AddColumnInfo cInfo = new AddColumnInfo(name, noCols, this, index, db);
				// make it visible
				cInfo.setVisible(true);	
			}
			this.dispose();
		}		
	}

	/**
	 * This method sets the text of 'Add Column Info' to 'Edit Column Info' after adding of column information.
	 * Also the method will check whether all addColButtons have labels 'Edit...' if so then the 'set key configuration'
	 * button can be added to the screen.
	 * @param index - index corresponding to the table being worked on
	 * @param colInfoAfterEdit - specific instance of AddColumnInfo which has been edited
	 */
	public void refreshAfterButtonReturn(int index, AddColumnInfo colInfoAfterEdit) {
		// this will update the button to edit once the column info is added
		addColButton.get(index).setText("Edit Column Info");

		// if the keyConfig is not included in buttonPanel try to add it
		if(!buttonPanel.isAncestorOf(fkConfig)){
			// check if the GUI is ready for the key config button to be added
			if(this.addButtons()) {
				buttonPanel.add(fkConfig);
				buttonPanel.add(genScripts);
			}
		}
		// remove the previous AddColumnInfo save 
		addColInfo.remove(index);
		// add the new AddColumnInfo
		addColInfo.add(index, colInfoAfterEdit);
		this.setVisible(true);
	}

	/**
	 * This method displays the Add Table Info screen and saves the instance of AddColumnInfo and the index
	 * to the array list so that it can be easily accessed when the user wants to return to this screen to 
	 * add the column information.
	 * @param index - this is the index corresponding to the table being worked on
	 * @param colInfo - specific instance of AddColumnInfo which has been edited 
	 */
	public void refreshAfterPreviousPageRequest(int index, AddColumnInfo colInfo) {
		/* the button name will be the same for addColButton in this case as 'back' has been pressed,
		 * and therefore no columns have been technically saved.
		 */
		if(addColInfo.get(index) != null){
			// remove the current save 
			addColInfo.remove(index);
		}
		addColInfo.add(index, colInfo);
		this.setVisible(true);
	}

	/**
	 * Populating the combo box with items - in this case the items are the number of columns 
	 * @param column - JComboBox to be populated
	 */
	public void populateComboBox(JComboBox column){
		int max_cols = 20;
		for(int i = 1; i <= max_cols; i++){
			column.addItem(i);
		}
		column.setSelectedIndex(-1);
	}

	/**
	 * This method looks at the label of the key configurator and accordingly addresses whether a new instance
	 * should be created or a previous instance should be again made visible.
	 */
	public void keyConfigurator() {
		String firstTable = db.getTables().get(0).getName();
		boolean last;
		if(db.getNoTables()==1){
			last = true;
		}
		else{
			last = false;
		}
		fKConfiguration = new FKConfiguration(firstTable, 0, last, db, this, null);
		fKConfiguration.setVisible(true);
		// dispose the current screen 
		this.dispose();
	}

	/**
	 * This method disposes the current screen and creates a new instance of Script Generator
	 */
	public void generateScripts() {
		// close the current screen
		this.dispose();
		ScriptGenerator scriptGenerator = new ScriptGenerator(db, this);
		scriptGenerator.setVisible(true);
	}

	/**
	 * This method is entered after the key configurations have been set. 
	 * @param configs - an array list of FKConfiguration holding an instances of 
	 * FKConfiguration for each table.
	 */
	public void refreshAfterFKConfig() {
		fkConfig.setText("Edit Foreign Keys");

		this.add(buttonPanel, BorderLayout.SOUTH);
		this.repaint();
		// set the current screen visible again
		this.setVisible(true);
	}

	/**
	 * This method takes into account a new number of tables and sets the JFrame accordingly.
	 * Control is passed to the addComponents helper method to add the relevant JComponents.
	 */
	public void refresh() {
		// set the layout manager to work with the new number of tables
		tableInfoPanel.setLayout(new GridLayout(db.getNoTables(), 5));

		/* 
		 * loop from original noTables to the new number of tables and add 
		 * additional rows for information
		 */
		addComponents(noTables, db.getNoTables());

		// set the noTables equal to the new number of tables
		this.noTables = db.getNoTables();

		// set the size of the screen with the new number of tables
		if(noTables == 1){
			this.setSize(900,100);
		}
		else{
			this.setSize(900,65*noTables);
		}

		// remove the 'add foreign keys' button if it is on the buttonPanel
		if(buttonPanel.isAncestorOf(fkConfig)){
			buttonPanel.remove(fkConfig);
		}
		if(buttonPanel.isAncestorOf(genScripts)){
			buttonPanel.remove(genScripts);
		}
	}


	@Override
	public void actionPerformed(ActionEvent ae) {
		for(int j = 0; j < noTables; j++){
			if(ae.getSource() == addColButton.get(j)){
				this.addColumnInfo(j);
				break;
			}
			else if(ae.getSource() == back){
				// move back to previous page
				this.dispose();
				init.refresh(this);
			}
			else if(ae.getSource() == fkConfig){
				this.keyConfigurator();
				// break so that we don't create more than one new FKConfiguration screen
				break;
			}
			else if(ae.getSource() == genScripts){
				this.generateScripts();
				break;
			}
		}
	}
}
