import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public abstract class GUI extends JFrame{

	protected JFrame mainFrame = new JFrame();
	protected JPanel panel;
	protected Database db = new Database();
	
	/**
	 * Constructor of abstract class GUI
	 */
	public GUI() {
		this.setSize(700,150);
		this.setLocation(100, 100);
		this.setTitle("SQL Generator");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public abstract void initialSetUp();
	public abstract void actionPerformed(ActionEvent ae);
}
