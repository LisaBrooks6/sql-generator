import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;
import org.chocosolver.solver.*;
import org.chocosolver.solver.variables.*;
import org.chocosolver.solver.constraints.*;
import org.chocosolver.solver.search.strategy.*;
import org.chocosolver.solver.exception.ContradictionException;


/*
 * The aim of this class is to deal with the constraints on columns with only 
 * primary keys, and columns with no key constraints at all. This class will only 
 * work for integers.
 */

public class IntegerSolver extends ConstraintSolver {
	private ArrayList<Integer> fileContent = new ArrayList<Integer>();

	/**
	 * Constructor for IntegerSolver
	 * @param fileName
	 * @param checkList
	 * @param unique
	 * @param notNull
	 * @param type
	 * @param noData
	 */
	public IntegerSolver(String fileName, ArrayList<Check> checkList, boolean unique, boolean notNull, String type, int noData) {
		this.fileName = fileName;
		this.checkList = checkList;
		this.type = type;
		this.noData = noData;
		this.unique = unique;
		this.notNull = notNull;
		
		solver = new Solver("int_solver");

		this.initialSetUp();
	}

	/**
	 * Method to set up the IntVar array.
	 */
	public void initialSetUp() {

		this.readFile();
		// move the values in fileContent into an int array 
		int [] content = new int[fileContent.size()];
		int i = 0;
		for(Integer data : fileContent) {
			// load the array position with the file content
			content[i] = data;
			// increment to the next position
			i++;
		}

		/* 
		 * the data array is of length noData. Each value 
		 * has domain of all values in content.
		 */
		data = VF.enumeratedArray("data", noData, content , solver);

		try {
			this.addConstraints();
		} catch (ContradictionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to add the constraints that the data IntVar array must satisfy.
	 */
	public void addConstraints() throws ContradictionException {
		// check constraint
		if(checkList != null) {
			for(Check c : checkList) {
				int val = Integer.parseInt(c.getValue().trim());
				// add a constraint for each check
				for(int i = 0; i < data.length; i++) {
					if(type.equals("Integer")) {
						solver.post(ICF.arithm(data[i], c.getCheckOperator().trim(), val));
					}
				}
			}
		}

		// unique
		if(unique) {
			solver.post(ICF.alldifferent(data));
		}
		// heuristics to ensure the data is a random value each time
		solver.set(ISF.random_value(data));
		// propagate
		try {
			solver.propagate();
		}
		catch(ContradictionException e) {
			// if it ends up in here then I want to end the loop 
			throw new ContradictionException();
		}
	}

	/**
	 * Method to read the input file.
	 */
	public void readFile() {
		// loop through the file and find the min and max values
		try {
			FileReader reader = new FileReader(fileName);
			Scanner sc = new Scanner(reader);
			while(sc.hasNextLine()) {
				// add to fileContent
				String next = sc.nextLine();
				if(notNull) {
					if(!next.equals("null")) {
						int content = Integer.parseInt(next);
						fileContent.add(content);
					}
				}
				else {
					// add the value regardless
					int content = Integer.parseInt(next);
					fileContent.add(content);
				}
			}
		}
		catch(FileNotFoundException | NumberFormatException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to return the outcome of whether a solution was found or not
	 * @return true/false 
	 */
	public boolean solve() {
		return solver.findSolution();
	}

	/**
	 * Method to return the solution which satisfies the constraints
	 * @return data
	 */
	public IntVar[] getData() {
		return data;
	}
}
