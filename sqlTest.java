import static org.junit.Assert.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import org.junit.Before;
import org.junit.Test;

public class sqlTest {
	private Database db;

	/* 
	 * Need to create the schema before being able to do any other tests.
	 */
	@Before
	public void createSchema() {
		System.out.println("Inside createSchema() before");
		// create a new instance of database 
		db = new Database();

		// set the name of the database
		db.setDbName("University");
		// set the number of tables
		db.setNoTables(5);

		// add a table - Student with 2 columns 
		db.addTable("Student", 3);
		// check that the test passes for each table being added 
		assertEquals("added", db.addColumnToTable("Student", "name", "VarChar", 30));
		assertEquals("added", db.addColumnToTable("Student", "matric", "Integer", null));
		assertEquals("added", db.addColumnToTable("Student", "advisor", "Integer", null));
		assertEquals("added", db.addColumnToTable("Student", "dob", "Date", null));

		ArrayList<Check> nameLengthCheck = new ArrayList<Check>();
		// check that the length of the name is 4
		nameLengthCheck.add(new Check("=", "4"));
		assertTrue(db.addConstraintsToColumn("Student", "name", true, true, true, nameLengthCheck, false, null));

		// add a check to ensure that the dob is before the year 2000 
		ArrayList<Check> dobCheck = new ArrayList<Check>();
		dobCheck.add(new Check("<", "2000"));
		assertTrue(db.addConstraintsToColumn("Student", "dob", true, false, true, dobCheck, false, null));

		// add the second table - Lecturer with 3 columns
		db.addTable("Lecturer", 3);
		// add the columns 
		assertEquals("added", db.addColumnToTable("Lecturer", "name", "VarChar", 30));
		assertEquals("added", db.addColumnToTable("Lecturer", "id", "Integer", null));
		assertEquals("added", db.addColumnToTable("Lecturer", "dept", "Character", 1));

		ArrayList<Check> surnameCheck = new ArrayList<Check>();
		// check that the length of the surname is between 2 and 7 but not equal to 4
		surnameCheck.add(new Check("<", "7"));
		surnameCheck.add(new Check("!=", "4"));
		surnameCheck.add(new Check(">", "2"));
		// test adding a default value and a check list
		assertTrue(db.addConstraintsToColumn("Lecturer", "name", false, false, true, surnameCheck, true, "Dr"));

		// test adding checks 
		ArrayList<Check> checks = new ArrayList<Check>();
		// check that the ID is between 1000000 and 2100000
		checks.add(new Check(">", "1000000"));
		checks.add(new Check("<", "2100000"));
		// test that checks is not empty
		assertNotEquals(0, checks.size());

		assertTrue(db.addConstraintsToColumn("Lecturer", "id", true, true, true, checks, false, null));

		// add another table called Dept with two columns
		db.addTable("Dept", 2);
		// add columns
		assertEquals("added", db.addColumnToTable("Dept", "name", "VarChar", 30));
		assertEquals("added", db.addColumnToTable("Dept", "letter", "Character", 1));
		// add a check to ensure the dept letter is before G
		ArrayList<Check> deptCheck = new ArrayList<Check>();
		deptCheck.add(new Check("<", "F"));
		assertTrue(db.addConstraintsToColumn("Dept", "letter", true, true, false, null, false, null));

		// add another table Course - 2 cols
		db.addTable("Course", 2);
		// add the columns 
		assertEquals("added", db.addColumnToTable("Course", "name", "VarChar", 30));
		assertEquals("added", db.addColumnToTable("Course", "id", "Integer", null));

		// add another table - Enrolment with 3 columns
		db.addTable("Enrolment", 3);
		// add the columns 
		assertEquals("added", db.addColumnToTable("Enrolment", "student", "Integer", null));
		assertEquals("added", db.addColumnToTable("Enrolment", "course", "Integer", null));
		assertEquals("added", db.addColumnToTable("Enrolment", "gpa", "Integer", null));

		// set the primary key of each table
		db.getTable("Student").getColumn("matric").setPrimaryKey(true);
		// ensure not null and unique
		assertTrue(db.addConstraintsToColumn("Student", "matric", true, true, false, null, false, null));
		// check that the primary key has been set for Student by checking the array list is of size 1
		assertEquals(1, db.getTable("Student").getPrimaryKey().size());

		db.getTable("Lecturer").getColumn("id").setPrimaryKey(true);
		// check that the primary key has been set for Lecturer, therefore check that the size of the array is 1
		assertEquals(1, db.getTable("Lecturer").getPrimaryKey().size());

		db.getTable("Dept").getColumn("letter").setPrimaryKey(true);
		// check that the primary key has been set for Dept, therefore check that the size of the array is 1
		assertEquals(1, db.getTable("Dept").getPrimaryKey().size());

		db.getTable("Course").getColumn("id").setPrimaryKey(true);
		assertTrue(db.addConstraintsToColumn("Course", "id", true, true, false, null, false, null));
		// check that the primary key has been set for Course, therefore check that the size of the array is 1
		assertEquals(1, db.getTable("Course").getPrimaryKey().size());

		db.getTable("Enrolment").getColumn("student").setPrimaryKey(true);
		assertTrue(db.addConstraintsToColumn("Enrolment", "student", true, true, false, null, false, null));
		db.getTable("Enrolment").getColumn("course").setPrimaryKey(true);
		assertTrue(db.addConstraintsToColumn("Enrolment", "course", true, true, false, null, false, null));
		// check there is a composite primary key comprising of 2 columns
		assertEquals(2, db.getTable("Enrolment").getPrimaryKey().size());

		// set the foreign key 'advisor' in Student
		db.getTable("Student").getColumn("advisor").setForeignKey(new ForeignKey(db.getTable("Lecturer"), db.getTable("Lecturer").getColumn("id")));
		// check the foreign key is not empty
		ForeignKey fk = db.getTable("Student").getColumn("advisor").getForeignKey();
		assertNotNull(fk);
		assertEquals("Lecturer", fk.getTable().getName());
		assertEquals("id", fk.getColumn().getName());

		// set the foreign key 'dept' in Lecturer
		db.getTable("Lecturer").getColumn("dept").setForeignKey(new ForeignKey(db.getTable("Dept"),  db.getTable("Dept").getColumn("letter")));
		
		db.getTable("Enrolment").getColumn("student").setForeignKey(new ForeignKey(db.getTable("Student"), db.getTable("Student").getColumn("matric")));
		db.getTable("Enrolment").getColumn("course").setForeignKey(new ForeignKey(db.getTable("Course"), db.getTable("Course").getColumn("id")));

		// check the foreign key is not null
		assertNotNull(db.getTable("Enrolment").getColumn("course").getForeignKey());
		assertNotNull(db.getTable("Enrolment").getColumn("student").getForeignKey());

		// Data Set
		// set the Student name data to be Names (concatenated)
		db.getTable("Student").getColumn("name").setDataSet("First Names");
		// set the Student matric data to be ID
		db.getTable("Student").getColumn("matric").setDataSet("Integers");
		// set the Student advisor data to be ID 
		db.getTable("Student").getColumn("advisor").setDataSet("ID (8 length integer)");

		// set the Lecturer name data to be Names (concatenated)
		db.getTable("Lecturer").getColumn("name").setDataSet("Surnames");
		// set the Lecturer id data to be ID
		db.getTable("Lecturer").getColumn("id").setDataSet("ID (8 length integer)");
		// set the Lecturer dept data to be Random Characters
		db.getTable("Lecturer").getColumn("dept").setDataSet("Random Characters");

		// set the Dept name data to be Course Names
		db.getTable("Dept").getColumn("name").setDataSet("Course Names");
		// set the Dept letter data to be Random Characters
		db.getTable("Dept").getColumn("letter").setDataSet("Random Characters");

		// set the Course name data to be Course Names
		db.getTable("Course").getColumn("name").setDataSet("Course Names");
		// set the Course id data to be integers
		db.getTable("Course").getColumn("id").setDataSet("Integers");

		// set the Enrolment student data to be ID 
		db.getTable("Enrolment").getColumn("student").setDataSet("ID (8 length integer)");
		// set the Enrolment course data to be integer
		db.getTable("Enrolment").getColumn("course").setDataSet("Integers");
		// set the Enrolment gpa data to be integer
		db.getTable("Enrolment").getColumn("gpa").setDataSet("Integers");

	}

	@Test
	public void testGeneratingSqlWithJoin() {
		// generate the data for the primary keys
		ArrayList<ArrayList<String>> pkData = db.populatePkData(10);
		// ensure that the arraylist is not null 
		assertNotNull(pkData);
		// set the primary key data in db
		db.setPkData(pkData);
		// ensure the size of the arraylist is not equal to 0
		assertNotEquals(0, db.getPkData().size());

		// generate the data for the foreign keys
		ArrayList<ArrayList<String>> fkData = db.populateFkData(pkData);
		// ensure that the arraylist is not null and of size not equal to 0
		assertNotNull(fkData);
		// set the foreign key data in db
		db.setFkData(fkData);
		// ensure the size of the arraylist is not equal to 0
		assertNotEquals(0, db.getFkData().size());

		// populate the population script so that the sql queries have data to work with
		db.generatePopulationScript(5, null);

		// set distinct, where, and, or, between, in, orderBy, asc, fullOuter, count, max equal to true
		db.setChosenSqlKeywords(true, true, true, true, true, true, false, false, 
				true, true, false, "fullOuter", true, false, false, false, false, true, false, false, false);

		// get the sql examples for the chosen data 
		ArrayList<String> examples = db.getSqlExamples();

		db.downloadExamplesFile(examples);

		ArrayList<String> fileContent = readFile("SQL_Examples.txt");

		// check that the file contains a full outer join 
		boolean join = false;
		for(String s: fileContent) {
			if(s.contains("FULL OUTER JOIN")) {
				join = true;
			}
		}
		// check that the join happens
		assertTrue(join);
		// check that the file is not empty 
		assertNotEquals(0, fileContent.size());
	}

	@Test
	public void generateSQLnoFKsNoPopScript() {

		// remove the foreign keys 
		db.getTable("Student").getColumn("advisor").setForeignKey(null);
		db.getTable("Lecturer").getColumn("dept").setForeignKey(null);
		db.getTable("Enrolment").getColumn("student").setForeignKey(null);
		db.getTable("Enrolment").getColumn("course").setForeignKey(null);

		// set distinct, where, and, or, between, in, equals orderBy, asc, fullOuter, count, max equal to true
		db.setChosenSqlKeywords(true, true, true, true, true, true, true, false, 
				true, true, false, "fullOuter", true, false, false, false, false, true, false, false, false);

		// get the sql examples for the chosen data 
		ArrayList<String> examples = db.getSqlExamples();
		db.downloadExamplesFile(examples);

		ArrayList<String> fileContent = readFile("SQL_Examples.txt");

		// check that the file does not contain a full outer join as there are no foreign keys
		boolean join = false;
		boolean value1 = false;
		for(String s: fileContent) {
			if(s.contains("FULL OUTER JOIN")) {
				join = true;
			}
			if(s.contains("VALUE_1")){
				value1 = true;
			}
		}

		// check that the join happens
		assertFalse(join);
		// check that the population script has not been generated and that the values are generic
		assertTrue(value1);

		// check that the file is not empty 
		assertNotEquals(0, fileContent.size());
	}


	@SuppressWarnings("resource")
	public ArrayList<String> readFile(String file) {
		ArrayList<String> fileContent = new ArrayList<String>();
		try{
			FileReader reader = new FileReader(file);
			Scanner sc = new Scanner(reader);

			while(sc.hasNextLine()) {
				fileContent.add(sc.nextLine());
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("File not found");
		}
		return fileContent;
	}
}
