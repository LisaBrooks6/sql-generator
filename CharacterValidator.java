import java.util.ArrayList;

public class CharacterValidator extends ConstraintValidator {
	
	ArrayList<Character> charSolution;
	ArrayList<Integer> solution = new ArrayList<Integer>();
	
	public CharacterValidator(ArrayList<Character> charSolution, ArrayList<Check> checkList, boolean unique, boolean notNull, int noData) {
		this.charSolution = charSolution;
		this.checkList = checkList;
		this.notNull = notNull;
		this.unique = unique;
		this.noData = noData;
		
		this.changeToInt();
	}
	
	public void changeToInt() {
		for(char c: charSolution) {
			int i = (int)c;
			solution.add(i);
		}
	}
	
	@Override
	protected boolean validate() {
		boolean pass = true;
		if(solution.size() != noData) {
			System.out.println("Character failed as the correct number of solutions was not found");
			pass = false;
		}
		if(checkList!=null)	{
			for(Check c: checkList) {
				String op = c.getCheckOperator();
				String value = c.getValue().trim();
				int val = (int)(value.charAt(0));
				if(checkValidator(op, val) == false) {
					System.out.println("Character failed on check: "+op+" "+val);
					pass = false;
				}
			}
		}
		if(notNull) {
			for(Integer i : solution) {
				if(i == null) {
					System.out.println("Character failed as null was found");
					pass = false;
				}
			}
		}
		if(unique) {
			ArrayList<Integer> uniqueValidation = new ArrayList<Integer>();
			for(Integer i: solution) {
				if(!uniqueValidation.contains(i)) {
					uniqueValidation.add(i);
				}
				else{
					// this already exists in the arraylist, hence not unique
					System.out.println("Character failed as duplicate was found");
					pass = false;
				}
			}
		}
		return pass;
	}

	public boolean checkValidator(String op, Integer val) {
		if(op.equals("<")) {
			for(Integer i: solution) {
				if(!(i < val)){
					return false;
				}
			}
		}
		else if(op.equals(">")) {
			for(Integer i: solution) {
				if(!(i > val)){
					return false;
				}
			}
		}
		else if(op.equals("=")) {
			for(Integer i: solution) {
				if(i != val){
					return false;
				}
			}
		}
		else if(op.equals("!=")) {
			for(Integer i: solution) {
				if(i == val){
					return false;
				}
			}
		}
		else if(op.equals("<=")) {
			for(Integer i: solution) {
				if(i > val){
					return false;
				}
			}
		}
		else if(op.equals(">=")) {
			for(Integer i: solution) {
				if(i < val){
					return false;
				}
			}
		}
		return true;
	}

}
