	
public class ForeignKey {
	private Table t;
	private Column c;
	
	/**
	 * Constructor for ForeignKey
	 * @param t - Table in the foreign key  
	 * @param c - Column in the foreign key
	 */
	public ForeignKey(Table t, Column c) {
		this.t = t;
		this.c = c;
	}

	/**
	 * Accessor for Table t
	 * @return t
	 */
	public Table getTable() {
		return t;
	}

	/**
	 * Mutator for Table t
	 * @param t
	 */
	public void setTable(Table t) {
		this.t = t;
	}

	/**
	 * Accessor for Column c
	 * @return c
	 */
	public Column getColumn() {
		return c;
	}

	/**
	 * Mutator for Column c
	 * @param c
	 */
	public void setColumn(Column c) {
		this.c = c;
	}
	
}
