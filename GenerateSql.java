import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class GenerateSql extends GUI implements ActionListener, ItemListener {

	private JPanel allJoinInfo, buttonPanel, checkPanel, checkLabelPanel, joinsPanel; 
	private JPanel distinctPanel, wherePanel, functionPanel, orderPanel, groupPanel;
	private JLabel checkDescription, joinDescription; 
	private JCheckBox distinct, where, and, or, between, in, equals, groupBy, orderBy;
	private JCheckBox count, sum, avg, first, last, max, min, ucase, lcase;
	private JRadioButton none, inner, fullOuter, left, right, asc, desc;
	private ButtonGroup joinRadios, orderRadios;
	private JButton back, getEx;

	private ScriptGenerator scriptGenerator;

	/**
	 * Constructor for GenerateSql
	 * @param scriptGenerator - instance of ScriptGenerator
	 * @param db - instance of Database holding the schema so far
	 */
	public GenerateSql(ScriptGenerator scriptGenerator, Database db) {
		// set the title
		this.setTitle("Find SQL Examples");
		this.setSize(900, 300);

		this.scriptGenerator = scriptGenerator;
		this.db = db;

		this.initialSetUp();
	}

	@Override
	public void initialSetUp() {
		// set up JPanels
		checkPanel = new JPanel(new GridLayout(7,1));
		distinctPanel = new JPanel(new GridLayout(1,5));
		wherePanel = new JPanel(new GridLayout(1,5));
		functionPanel = new JPanel(new GridLayout(1,9));
		orderPanel = new JPanel(new GridLayout(1,5));
		groupPanel = new JPanel(new GridLayout(1,5));

		checkLabelPanel = new JPanel();

		allJoinInfo = new JPanel(new GridLayout(2,1));
		joinsPanel = new JPanel();
		buttonPanel = new JPanel();

		// set up JLabels
		checkDescription = new JLabel("Find SQL examples including...");
		joinDescription = new JLabel("Choose a type of join: ");

		// set up checkboxes
		distinct = new JCheckBox("DISTINCT");
		where = new JCheckBox("WHERE");
		and = new JCheckBox("AND");
		or = new JCheckBox("OR");
		between = new JCheckBox("BETWEEN");
		in = new JCheckBox("IN");
		equals = new JCheckBox("EQUALS");
		count = new JCheckBox("COUNT");
		sum = new JCheckBox("SUM");
		avg = new JCheckBox("AVG");
		first = new JCheckBox("FIRST");
		last = new JCheckBox("LAST");
		max = new JCheckBox("MAX");
		min = new JCheckBox("MIN");
		ucase = new JCheckBox("UCASE");
		lcase = new JCheckBox("LCASE");
		groupBy = new JCheckBox("GROUP BY");
		orderBy = new JCheckBox("ORDER BY");

		// add action listeners to the checkboxes
		distinct.addActionListener(this);
		where.addActionListener(this);
		and.addActionListener(this);
		or.addActionListener(this);
		between.addActionListener(this);
		in.addActionListener(this);
		equals.addActionListener(this);
		count.addActionListener(this);
		sum.addActionListener(this);
		avg.addActionListener(this);
		first.addActionListener(this);
		last.addActionListener(this);
		max.addActionListener(this);
		min.addActionListener(this);
		ucase.addActionListener(this);
		lcase.addActionListener(this);
		groupBy.addActionListener(this);
		orderBy.addActionListener(this);
		// add item listeners
		where.addItemListener(this);
		orderBy.addItemListener(this);

		// set up radio buttons
		none = new JRadioButton("None");
		inner = new JRadioButton("Inner Join");
		fullOuter = new JRadioButton("Full Outer Join");
		left = new JRadioButton("Left Join");
		right = new JRadioButton("Right Join");

		asc = new JRadioButton("ASC");
		desc = new JRadioButton("DESC");

		// set up button group and add buttons
		joinRadios = new ButtonGroup();
		joinRadios.add(none);
		joinRadios.add(inner);
		joinRadios.add(fullOuter);
		joinRadios.add(left);
		joinRadios.add(right);

		orderRadios = new ButtonGroup();
		orderRadios.add(asc);
		orderRadios.add(desc);

		// add action listeners to radio buttons 
		none.addActionListener(this);
		inner.addActionListener(this);
		fullOuter.addActionListener(this);
		left.addActionListener(this);
		right.addActionListener(this);

		asc.addActionListener(this);
		desc.addActionListener(this);

		// set up the buttons
		back = new JButton("Back");
		getEx = new JButton("Get Examples");

		// add action listener to buttons
		back.addActionListener(this);
		getEx.addActionListener(this);

		// set and, between, or, in, asc and desc not enabled
		this.disableCheckBoxes();
		this.disableRadioButtons();

		// add components to panels
		checkLabelPanel.add(checkDescription);

		distinctPanel.add(distinct);
		wherePanel.add(where);
		wherePanel.add(and);
		wherePanel.add(or);
		wherePanel.add(between);
		wherePanel.add(in);
		wherePanel.add(equals);

		functionPanel.add(count);
		functionPanel.add(sum);
		functionPanel.add(avg);
		functionPanel.add(first);
		functionPanel.add(last);
		functionPanel.add(max);
		functionPanel.add(min);
		functionPanel.add(ucase);
		functionPanel.add(lcase);

		groupPanel.add(groupBy);
		orderPanel.add(orderBy);
		orderPanel.add(asc);
		orderPanel.add(desc);

		// add the panels to the checkPanel
		checkPanel.add(checkLabelPanel);
		checkPanel.add(distinctPanel);
		checkPanel.add(wherePanel);
		checkPanel.add(functionPanel);
		checkPanel.add(groupPanel);
		checkPanel.add(orderPanel);

		// add components to joinsPanel
		joinsPanel.add(joinDescription);
		joinsPanel.add(none);
		joinsPanel.add(inner);
		joinsPanel.add(fullOuter);
		joinsPanel.add(left);
		joinsPanel.add(right);

		// add join related panels to baseJoins
		allJoinInfo.add(joinsPanel);

		// add the back and get examples button to the buttonPanel
		buttonPanel.add(back);
		buttonPanel.add(getEx);

		this.add(checkPanel, BorderLayout.NORTH);
		this.add(allJoinInfo, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Accessor method for ScriptGenerator
	 * @return scriptGenerator
	 */
	public ScriptGenerator getScriptGenerator() {
		return scriptGenerator;
	}
	
	/**
	 * This method captures the input and sends it to be set in an instance of ChosenSqlKeywords. Control is then 
	 * passed to Database to produce the SQL examples which are subsequently saved in an arraylist and sent onto
	 * generateExampleScreen().
	 */
	public void captureInput(){
		String selectedJoin = "";
		ArrayList<Boolean> validationPassed = new ArrayList<Boolean>();

		// get the type of join selected
		if(inner.isSelected()) {
			selectedJoin = "inner";
		}
		else if(fullOuter.isSelected()) {
			selectedJoin = "fullOuter";
		}
		else if(left.isSelected()) {
			selectedJoin = "left";
		}
		else if(right.isSelected()) {
			selectedJoin = "right";
		}
		else if(none.isSelected()) {
			selectedJoin = "none";
		}

		if(!where.isSelected()) {
			// set all the operators that work with where false: and, or, between, in
			between.setSelected(false);
			in.setSelected(false);
			or.setSelected(false);
			and.setSelected(false);
			equals.setSelected(false);
		}

		/*
		 * If Order By has been selected then we want to check that either ASC or DESC has been selected. Else, deselect these
		 * options.
		 */
		if(!orderBy.isSelected()) {
			// deselect asc and desc
			orderRadios.clearSelection();
		}
		else {
			if(!asc.isSelected() && !desc.isSelected()) {
				// if neither ASC or DESC are chosen, raise an alert
				JOptionPane.showMessageDialog(null, "Please choose an operator to order by","Error Message", JOptionPane.ERROR_MESSAGE);
				validationPassed.add(false);
			}
		}

		/*
		 * If there has been no chosen operators, raise an alert to the user. Else, check that there were no validation errors and
		 * continue to generate the SQL.
		 */
		if((!distinct.isSelected() && !where.isSelected() && !and.isSelected() && !or.isSelected() && !between.isSelected() && !in.isSelected() && !equals.isSelected()
				&& !groupBy.isSelected() && !orderBy.isSelected() && !asc.isSelected() && !desc.isSelected() && !count.isSelected() && !sum.isSelected()
				&& !avg.isSelected() && !first.isSelected() && !last.isSelected() && !max.isSelected() && !min.isSelected() && !ucase.isSelected() && !lcase.isSelected()) 
				|| selectedJoin.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please provide operator choices and a choice of join", "Error Message", JOptionPane.ERROR_MESSAGE);
			validationPassed.add(false);
		}
		else{ 
			if(!validationPassed.contains(false)){
				db.setChosenSqlKeywords(distinct.isSelected(), where.isSelected(), and.isSelected(), 
						or.isSelected(), between.isSelected(), in.isSelected(), equals.isSelected(), groupBy.isSelected(), 
						orderBy.isSelected(), asc.isSelected(), desc.isSelected(), selectedJoin,
						count.isSelected(), sum.isSelected(), avg.isSelected(), first.isSelected(), last.isSelected(), max.isSelected(), min.isSelected(), 
						ucase.isSelected(), lcase.isSelected());

				ChosenSqlKeywords chosen = new ChosenSqlKeywords(distinct.isSelected(), where.isSelected(), and.isSelected(), 
						or.isSelected(), between.isSelected(), in.isSelected(), equals.isSelected(), groupBy.isSelected(), 
						orderBy.isSelected(), asc.isSelected(), desc.isSelected(), selectedJoin, 
						count.isSelected(), sum.isSelected(), avg.isSelected(), first.isSelected(), last.isSelected(), max.isSelected(), min.isSelected(), 
						ucase.isSelected(), lcase.isSelected());

				ArrayList<String> examples = db.getSqlExamples();
				generateExampleScreen(examples);
			}
		}
	}

	/**
	 * This method creates an instance of DisplayExamples, sets this visible and disposes the current screen
	 * @param examples - ArrayList holding the SQL examples
	 */
	public void generateExampleScreen(ArrayList<String> examples) {
		DisplayExamples displayEx = new DisplayExamples(examples, this, db);
		displayEx.setVisible(true);
		this.dispose();
	}

	/**
	 * This method makes the scriptGenerator instance visible (the previous page) and disposes 
	 * the current screen.
	 */
	public void showPreviousScreen() {
		scriptGenerator.setVisible(true);
		this.dispose();
	}

	/**
	 * This method enables the checkBoxes on the screen
	 */
	public void enableCheckBoxes() {
		and.setEnabled(true);
		between.setEnabled(true);
		or.setEnabled(true);
		in.setEnabled(true);
		equals.setEnabled(true);
	}

	/**
	 * This method disables the checkBoxes on the screen from being used
	 */
	public void disableCheckBoxes() {
		and.setEnabled(false);
		between.setEnabled(false);
		or.setEnabled(false);
		in.setEnabled(false);
		equals.setEnabled(false);
	}

	/**
	 * This method disables the use of the radio buttons on the screen
	 */
	public void disableRadioButtons() {
		asc.setEnabled(false);
		desc.setEnabled(false);
	}

	/**
	 * This method enables the use of the radio buttons
	 */
	public void enableRadioButtons() {
		asc.setEnabled(true);
		desc.setEnabled(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == back) {
			this.showPreviousScreen();
		}
		else if(ae.getSource() == getEx) {
			this.captureInput();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getItemSelectable() == where) {
			if(e.getStateChange() == e.SELECTED) {
				enableCheckBoxes();
			}
			else{
				disableCheckBoxes();
			}
		}
		if(e.getItemSelectable() == orderBy) {
			if(e.getStateChange() == e.SELECTED) {
				enableRadioButtons();
			}
			else{
				disableRadioButtons();
			}
		}
	}
}
