import java.util.ArrayList;

public class Column {

	private String name, defaultVal, dataSet;
	private Object type;
	private Integer size;
	private boolean notNull, unique, check, defaultCheck, primaryKey, constraintsAdded = false; // originally
	private ForeignKey foreignKey;
	private ArrayList<Check> checkList;

	/**
	 * Constructor for Column
	 * @param name - String for column name
	 * @param type - Object for column type
	 * @param size - Integer for column size
	 */
	public Column(String name, Object type, Integer size) {
		this.name = name;
		this.type = type;
		this.size = size;
	}

	/**
	 * Constructor if we know the constraints at the time of creation. This will not be used in this implementation,
	 * however has been included for future development to make the code more flexible.
	 * @param name
	 * @param type
	 * @param size
	 * @param notNull
	 * @param unique
	 * @param check
	 * @param defaultCheck
	 * @param defaultVal
	 */
	public Column(String name, Object type, Integer size, boolean notNull, boolean unique, boolean check, ArrayList<Check> checks, boolean defaultCheck, String defaultVal){
		this.name = name;
		this.type = type;
		this.size = size;
		this.notNull = notNull;
		this.unique = unique;
		this.check = check;
		this.checkList = checks;
		this.defaultCheck = defaultCheck;
		this.defaultVal = defaultVal;
	}

	/**
	 * Accessor for defaultVal
	 * @return defaultVal - String containing the default value of the column
	 */
	public String getDefaultVal() {
		return defaultVal;
	}
	
	/**
	 * Mutator for defaultVal
	 * @param defaultVal
	 */
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}
	
	/**
	 * Accessor for type 
	 * @return type
	 */
	public Object getType() {
		return type;
	}

	/**
	 * Mutator for type
	 * @param type
	 */
	public void setType(Object type) {
		this.type = type;
	}
	
	/**
	 * Accessor for size
	 * @return size
	 */
	public Integer getSize() {
		return size;
	}
	
	/**
	 * Mutator for size
	 * @param size
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	
	/**
	 * Accessor for notNull
	 * @return notNull
	 */
	public boolean isNotNull() {
		return notNull;
	}
	
	/**
	 * Mutator for notNull
	 * @param notNull
	 */
	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}
	
	/**
	 * Accessor for unique
	 * @return unique
	 */
	public boolean isUnique() {
		return unique;
	}
	
	/**
	 * Mutator for unique
	 * @param unique
	 */
	public void setUnique(boolean unique) {
		this.unique = unique;
	}
	
	/**
	 * Accessor for check
	 * @return check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Mutator for check 
	 * @param check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Accessor for defaultCheck
	 * @return defaultCheck
	 */
	public boolean isDefaultCheck() {
		return defaultCheck;
	}
	
	/**
	 * Mutator for defaultCheck
	 * @param defaultCheck
	 */
	public void setDefaultCheck(boolean defaultCheck) {
		this.defaultCheck = defaultCheck;
	}

	/**
	 * Accessor for checkList
	 * @return checkList
	 */
	public ArrayList<Check> getCheckList() {
		return checkList;
	}
	
	/**
	 * Mutator for checkList
	 * @param checkList
	 */
	public void setCheckList(ArrayList<Check> checkList) {
		this.checkList = checkList;
	}

	/**
	 * Mutator for name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Accessor for name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Accessor for primaryKey
	 * @return primaryKey
	 */
	public boolean isPrimaryKey() {
		return primaryKey;
	}
	
	/**
	 * Mutator for primaryKey
	 * @param primaryKey
	 */
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
		/* if the primaryKey is true, then these will be true, else they 
		 * will be false unless explicitly set.
		 */
		this.setNotNull(primaryKey);
		this.setUnique(primaryKey);
	}
	
	/**
	 * Accessor for constraintsAdded
	 * @return constraintsAdded
	 */
	public boolean getConstraintsAdded() {
		return constraintsAdded;
	}

	/**
	 * Mutator for constraintsAdded
	 * @param constraintsAdded
	 */
	public void setConstraintsAdded(boolean constraintsAdded) {
		this.constraintsAdded = constraintsAdded;
	}

	/**
	 * Accessor for foreignKey
	 * @return foreignKey
	 */
	public ForeignKey getForeignKey() {
		return foreignKey;
	}
	
	/**
	 * Mutator for foreignKey
	 * @param fk - the foreign key of the column
	 */
	public void setForeignKey(ForeignKey fk) {
		foreignKey = fk;
	}
	
	/**
	 * Mutator for dataSet
	 * @param dataSet
	 */
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	/**
	 * Accessor for dataSet
	 * @return dataSet
	 */
	public String getDataSet() {
		return dataSet;
	}
}