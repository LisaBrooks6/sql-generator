import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ScriptGenerator extends GUI implements ActionListener {
	
	private JButton downloadCreate, genPop, genSql, startAgain, back, exit;
	private JPanel buttonPanelCentre, buttonPanelSouth;
	private AddTableInfo tInfo;
	
	/**
	 * Constructor for ScriptGenerator
	 * @param db - instance of Database
	 * @param tInfo - instance of AddTableInfo
	 */
	public ScriptGenerator(Database db, AddTableInfo tInfo) {
		this.db = db;
		this.tInfo = tInfo;
		
		this.setTitle("SQL Generator: Generate Scripts");
		this.initialSetUp();
	}
	
	@Override
	public void initialSetUp() {
		downloadCreate = new JButton("Download Create Script");
		downloadCreate.addActionListener(this);
		genPop = new JButton("Generate Population Script");
		genPop.addActionListener(this);
		genSql = new JButton("Generate SQL");
		genSql.addActionListener(this);
		startAgain = new JButton("Start Schema Again");
		startAgain.addActionListener(this);
		back = new JButton("Back");
		back.addActionListener(this);
		exit = new JButton("Exit");
		exit.addActionListener(this);
		
		buttonPanelCentre = new JPanel(new GridLayout(1,3));
		buttonPanelSouth = new JPanel();
		
		buttonPanelCentre.add(downloadCreate);
		buttonPanelCentre.add(genPop);
		buttonPanelCentre.add(genSql);
		buttonPanelSouth.add(back);
		buttonPanelSouth.add(startAgain);
		buttonPanelSouth.add(exit);
		
		this.add(buttonPanelCentre, BorderLayout.CENTER);
		this.add(buttonPanelSouth, BorderLayout.SOUTH);	
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource()==downloadCreate){
			this.downloadCreateScript();
		}
		else if(ae.getSource()==genPop){
			this.generatePopScript();
		}
		else if(ae.getSource()==genSql){
			this.generateSql();
		}
		else if(ae.getSource()==startAgain){
			this.dispose();
			new InitialScreen(new Database()).setVisible(true);
		}
		else if(ae.getSource()==back){
			this.dispose();
			tInfo.setVisible(true);
		}
		else if(ae.getSource()==exit){
			System.exit(0);
		}
	}

	/**
	 * This method sends to Database to generate and download the create script
	 */
	public void downloadCreateScript() {
		db.downloadCreateScript();
	}
	
	/**
	 * This method disposes the current screen and creates a new instance of GeneratePopulationScript 
	 * and makes this visible.
	 */
	public void generatePopScript(){
		this.dispose();
		GeneratePopulationScript genPopScript = new GeneratePopulationScript(this, -1, db, null);
		genPopScript.setVisible(true);
	}
	
	/**
	 * This method disposes the current screen and creates a new instance of GenerateSql
	 * and makes this visible.
	 */
	public void generateSql(){
		this.dispose();
		GenerateSql sqlGen = new GenerateSql(this, db);
		sqlGen.setVisible(true);
	}
	
	public AddTableInfo getTInfo() {
		return tInfo;
	}
}
