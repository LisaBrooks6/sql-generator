import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.ICF;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.strategy.ISF;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.VF;

public class CharacterSolver extends ConstraintSolver {

	private ArrayList<Character> fileContent = new ArrayList<Character>();

	/**
	 * Constructor for CharacterSolver
	 * @param fileName
	 * @param checkList
	 * @param unique
	 * @param notNull
	 * @param noData
	 */
	public CharacterSolver(String fileName, ArrayList<Check> checkList, boolean unique, boolean notNull, int noData) {
		this.fileName = fileName;
		this.checkList = checkList;
		this.unique = unique;
		this.notNull = notNull;
		this.noData = noData;

		this.solver = new Solver("char_solver");
		this.initialSetUp();
	}

	/**
	 * Method to set up the IntVar array.
	 */
	public void initialSetUp() {
		this.readFile();
		// move the lengths of the strings in fileContent into an int array 
		int [] content = new int[fileContent.size()];
		int i = 0;
		for(char input : fileContent) {
			// change the char to its ascii value and load into content
			content[i] = (int)input;
			// increment to the next position
			i++;
		}
		
		/* 
		 * the data array is of length noData. Each value 
		 * has domain of all values in content.
		 */
		data = VF.enumeratedArray("data", noData, content, solver);
		// add the constraints
		try {
			this.addConstraints();
		} catch (ContradictionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to read the input file.
	 */
	public void readFile() {
		// loop through the file and find the min and max values
		try {
			FileReader reader = new FileReader(fileName);
			Scanner sc = new Scanner(reader);
			while(sc.hasNextLine()) {
				String content = sc.nextLine();
				// add all characters in content
				if(notNull) {
					if(!content.equals("null")) {
						// not equal to null value
						for(int i = 0; i<content.length(); i++) {
							fileContent.add(content.charAt(i));
						}
					}
				}
				else{ 
					for(int i = 0; i<content.length(); i++) {
						fileContent.add(content.charAt(i));
					}
				}
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("The file "+fileName+" was not found");
		}
	}

	/**
	 * Method to add the constraints that the data IntVar array must satisfy.
	 */
	public void addConstraints() throws ContradictionException {
		// check constraint
		if(checkList != null) {
			for(Check c : checkList) {
				String input = c.getValue().trim();
				// input should always be of length 1 due to restricting user input
				Character inputChar = (char)input.charAt(0);
				// change the char to its ascii value
				int val = (int)inputChar;
				// add a constraint for each check
				for(int i = 0; i < data.length; i++) {
					solver.post(ICF.arithm(data[i], c.getCheckOperator().trim(), val));
				}
			}
		}

		// unique
		if(unique) {
			solver.post(ICF.alldifferent(data));
		}
		// heuristics to ensure the data is a random value each time
		solver.set(ISF.random_value(data));
		// propagate
		try {
			solver.propagate();
		}
		catch(ContradictionException e) {
			// if it ends up in here then I want to end the loop 
			throw new ContradictionException();
		}
	}

	/**
	 * Method to return the outcome of whether a solution was found or not
	 * @return true/false 
	 */
	public boolean solve() {
		return solver.findSolution();
	}
	
	/**
	 * Method to return the solution which satisfies the constraints
	 * @return data
	 */
	public IntVar[] getData() {
		return data;
	}
	
	/**
	 * Method to return an arraylist of corresponding characters to the solution.
	 * @return chars
	 */
	public ArrayList<Character> getCharacterData() {
		ArrayList<Character> chars = new ArrayList<Character>();
		for(IntVar val : data) {
			// change the ascii value to a character
			char c = (char)val.getValue();
			chars.add(c);
		}
		return chars;
	}
}
