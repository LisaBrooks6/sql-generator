import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.RenderingHints.Key;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class FKConfiguration extends GUI implements ActionListener {
	private JLabel [] colNames;
	private JLabel emptyLabel, fkLabel, tableLabel, colLabel, tableNameLabel;
	private ArrayList<JCheckBox> fkCheckBoxes;
	private ArrayList<JComboBox> tableCombos, colCombos;
	private JButton next, saveReturn, back, previousTable;
	private JPanel tableNamePanel, keyGrid, buttonPanel;

	private String tableName;
	private int tableIndex, noCols;
	private boolean lastEntry;
	private AddTableInfo tInfo;
	private FKConfiguration fkConfig;
	
	/**
	 * Constructor for FKConfiguration
	 * @param tableName
	 * @param tableIndex
	 * @param lastEntry
	 * @param db
	 * @param tInfo
	 * @param configs
	 */
	public FKConfiguration(String tableName, int tableIndex, boolean lastEntry, Database db, AddTableInfo tInfo, FKConfiguration fkConfig) {
		this.tableName = tableName;
		this.tableIndex = tableIndex;
		this.lastEntry = lastEntry;
		this.db = db;
		this.tInfo = tInfo;
		this.fkConfig = fkConfig;
		
		this.setTitle("Schema Generation Key Configuration for table: "+tableName);
		this.initialSetUp();
	}

	@Override
	public void initialSetUp() {
		if(db.getTable(tableName).getNoCols() == 1){
			this.setSize(900,150);
		}
		else{
			this.setSize(900, 80*db.getTable(tableName).getNoCols());
		}
		
		// get the number of columns in the table. Due to set up, tableName will never be null
		noCols = db.getTable(tableName).getNoCols();

		tableNamePanel = new JPanel();
		tableNameLabel = new JLabel("Table = "+tableName.toUpperCase());
		// get the current font, make it bold, set the new font.
		Font font = tableNameLabel.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		tableNameLabel.setFont(boldFont);
		
		tableNamePanel.add(tableNameLabel);
		
		// initialise all components
		emptyLabel = new JLabel("");
		colNames = new JLabel[noCols];
		fkCheckBoxes = new ArrayList<JCheckBox>();
		tableCombos = new ArrayList<JComboBox>();
		colCombos = new ArrayList<JComboBox>();
		fkLabel = new JLabel("Foreign Key");
		tableLabel = new JLabel("Table");
		colLabel = new JLabel("Column");
		next = new JButton("Next Table");
		saveReturn = new JButton("Save and Return");
		back = new JButton("Back");
		previousTable = new JButton("Previous Table");

		keyGrid = new JPanel(new GridLayout(noCols+1, 5));
		buttonPanel = new JPanel();

		// add action listeners
		next.addActionListener(this);
		saveReturn.addActionListener(this);
		back.addActionListener(this);
		previousTable.addActionListener(this);
		
		// adding to JPanels 
		keyGrid.add(emptyLabel);
		keyGrid.add(fkLabel);
		keyGrid.add(tableLabel);
		keyGrid.add(colLabel);
		
		for(int i = 0; i < noCols; i++){
			colNames[i] = new JLabel(db.getTables().get(tableIndex).getColumns().get(i).getName());
			keyGrid.add(colNames[i]);

			fkCheckBoxes.add(i, new JCheckBox());
			fkCheckBoxes.get(i).addActionListener(this);
			keyGrid.add(fkCheckBoxes.get(i));

			tableCombos.add(i, new JComboBox());
			populateTableCombo(i);
			tableCombos.get(i).setEnabled(false);
			tableCombos.get(i).addActionListener(this);		
			keyGrid.add(tableCombos.get(i));

			colCombos.add(i, new JComboBox());
			colCombos.get(i).setEnabled(false);
			keyGrid.add(colCombos.get(i));

			// populate the foreign keys if they exist
			this.populateForeignKeys(i);
		}
		if(lastEntry){
			buttonPanel.add(previousTable);
			buttonPanel.add(saveReturn);
		}
		else if(tableIndex == 0) {
			// this is the first table config
			buttonPanel.add(back);
			buttonPanel.add(next);
		}
		else{
			buttonPanel.add(previousTable);
			buttonPanel.add(next);
		}
		this.add(tableNamePanel, BorderLayout.NORTH);
		this.add(keyGrid, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * This method will populate the fields with any previously set foreign keys
	 */
	public void populateForeignKeys(int i) {
		if(db.getTables().get(tableIndex).getColumns().get(i).getForeignKey() != null) {
			// this column has a foreign key
			fkCheckBoxes.get(i).setSelected(true);   
			tableCombos.get(i).setSelectedItem(db.getTables().get(tableIndex).getColumns().get(i).getForeignKey().getTable().getName().trim());
			colCombos.get(i).setSelectedItem(db.getTables().get(tableIndex).getColumns().get(i).getForeignKey().getColumn().getName().trim());
		}
	}

	/**
	 * This method populates the table combo boxes with the tables in the schema
	 * @param index - this is an int which corresponds to the column whose keys are being set 
	 */
	public void populateTableCombo(int index) {
		// this will include all tables
		for(int i = 0; i < db.getNoTables(); i++){
			tableCombos.get(index).addItem(db.getTables().get(i).getName());
		}
		tableCombos.get(index).setSelectedIndex(-1);
	}

	/**
	 * This method takes in the the table chosen and populates the column combo box with the 
	 * column names.
	 * @param tableName - String containing the name of the table chosen
	 * @param index - index corresponding to the column whose keys are being set
	 */
	public void populateColumnCombo(String tableName, int index) {
		if(!tableName.equals("")){
			String primaryKey = "";
			ArrayList<String> pk = new ArrayList<String>();
			pk = db.getTable(tableName).getPrimaryKey();
			for(String s : pk) {
				colCombos.get(index).addItem(s);
			}
			colCombos.get(index).setEnabled(true);
			colCombos.get(index).setSelectedIndex(-1);
		}
	}

	/**
	 * This method hands control over to the FKConfiguration instance for the next table in the 
	 * schema. If this does not exist then it is created.
	 */
	public void getNextConfiguration() {
		// save the configuration
		if(!this.saveConfiguration().contains(false)){
			// check if the next table is the last table
			boolean last;
			if(tableIndex+1 == db.getNoTables()-1){
				last = true;
			}
			else{
				last = false;
			}
			// create a new instance of FKConfiguration
			FKConfiguration fk = new FKConfiguration(db.getTables().get(tableIndex+1).getName().trim(), tableIndex+1, last, db, tInfo, this);
			fk.setVisible(true);
			// dispose the screen
			this.dispose();
		}
	}

	/**
	 * This method is entered when the last configuration screen has been completed and 'save and return' has
	 * been pressed. This saves the entered data using saveConfiguration() and sends configs (all saved FKConfiguration 
	 * instances) along with control over to tInfo (instance of AddTableInfo).
	 */
	public void saveLastConfiguration(){
		// this will be entered when save and return is pressed on the last table key config
		saveConfiguration();
		// close the current screen
		this.dispose();
		// refresh the table info screen
		tInfo.refreshAfterFKConfig();
	}

	/**
	 * This method captures all user input and saves the instance of FKConfiguration in the arraylist configs.
	 */
	public ArrayList<Boolean> saveConfiguration() {
		ArrayList<Boolean> configPassed = new ArrayList<Boolean>();
		// initialise the arraylist to be of length noCols and have values null to start with
		for(int j = 0; j < noCols; j++){
			configPassed.add(j, null);
		}
		
		// save info from this table
		for(int i = 0; i < noCols; i++){
			// gather the fk, table and column info
			boolean fk = fkCheckBoxes.get(i).isSelected();
			if(fk){
				// get the table
				String tableName = "";
				String colName = "";
				if(tableCombos.get(i).getSelectedIndex() < 0) {
					JOptionPane.showMessageDialog(null, "Please choose the table corresponding to your foreign key", "Empty Table Error", JOptionPane.ERROR_MESSAGE);
					configPassed.add(i, false);
				}
				else{
					tableName = tableCombos.get(i).getSelectedItem().toString().trim();
					// get the column
					if(colCombos.get(i).getSelectedIndex() < 0) {
						JOptionPane.showMessageDialog(null, "Please choose the column corresponding to your foreign key", "Empty Column Error", JOptionPane.ERROR_MESSAGE);
						configPassed.add(i, false);
					}
					else{
						colName = colCombos.get(i).getSelectedItem().toString().trim();
						// add the foreign key
						db.getTables().get(tableIndex).getColumns().get(i).setForeignKey(new ForeignKey(db.getTable(tableName), db.getTable(tableName).getColumn(colName)));
						configPassed.add(i, true);
					}
				}
			}
			else{
				// no foreign key has been chosen for this column, therefore set the foreign key as null
				db.getTables().get(tableIndex).getColumns().get(i).setForeignKey(null);
			}
		}
		return configPassed;
	}

	/**
	 * This method disposes the current screen and makes visible the FKConfiguration screen for 
	 * the previous tableIndex.
	 */
	public void returnToPreviousTable() {
		if(tableIndex-1 >= 0){
			this.dispose();
			fkConfig.setVisible(true);
		}
	}

	/**
	 * This method disposes the current screen and returns the user to the Table Info Screen.
	 * Nothing has been saved when this button is pressed.
	 */
	public void returnToTableInfoScreen() {
		this.dispose();
		// nothing has been saved yet, therefore just make the screen visible again
		tInfo.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == next){
			// new FKConfiguration and save info
			this.getNextConfiguration();
		}
		else if(ae.getSource() == saveReturn){
			// dispose screen and save entered info
			saveLastConfiguration();
		}
		else if(ae.getSource() == back) {
			this.returnToTableInfoScreen();
		}
		else if(ae.getSource() == previousTable) {
			this.returnToPreviousTable();
		}
		for(int i = 0; i < tableCombos.size(); i++){
			if(ae.getSource() == tableCombos.get(i)){
				// clear col combo before selecting a new table
				colCombos.get(i).removeAllItems();
				populateColumnCombo(tableCombos.get(i).getSelectedItem().toString(), i);
			}
			else if(ae.getSource()==fkCheckBoxes.get(i)){
				// make the table and column combo boxes editable/not editable
				if(fkCheckBoxes.get(i).isSelected()){
					tableCombos.get(i).setEnabled(true);
				}
				else{
					tableCombos.get(i).setEnabled(false);
				}
			}
		}
	}
}
