import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DisplayExamples extends GUI implements ActionListener {
	
	private ArrayList<String> examples;
	private GenerateSql genSql;
	
	private JTextArea examplesArea;
	private JScrollPane scroller;
	private JButton moreEx, downloadEx, backToScriptGen, exit;

	private JPanel buttonPanel;
	
	/**
	 * Constructor for DisplayExamples 
	 * @param examples
	 * @param genSql
	 * @param db
	 */
	public DisplayExamples(ArrayList<String> examples, GenerateSql genSql, Database db) {
		this.examples = examples;
		this.genSql = genSql;
		this.db = db;
		
		this.setSize(800, 200);
		this.initialSetUp();
	}

	@Override
	public void initialSetUp() {
		// set up components 
		buttonPanel = new JPanel(new GridLayout(1,3));
		
		examplesArea = new JTextArea();
		examplesArea.setEditable(false);
		// add scroll functionality to the text area
		scroller = new JScrollPane(examplesArea);
		
		moreEx = new JButton("More Examples");
		downloadEx = new JButton("Download Examples");
		backToScriptGen = new JButton("Return to Script Generator");
		exit = new JButton("Exit");
			
		buttonPanel.add(moreEx);
		buttonPanel.add(downloadEx);
		buttonPanel.add(backToScriptGen);
		buttonPanel.add(exit);
		
		moreEx.addActionListener(this);
		downloadEx.addActionListener(this);
		backToScriptGen.addActionListener(this);
		exit.addActionListener(this);
		
		for(int i = 0; i < examples.size(); i++){
			examplesArea.append(examples.get(i).toString()+"\n");
		}
		this.add(scroller, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * This method makes the previous screen to generate sql visible again and disposes the current screen
	 */
	public void returnToExampleFinder() {
		this.dispose();
		genSql.setVisible(true);
	}
	
	/**
	 * This method closes the current screen and opens the script generator screen.
	 */
	public void returnToScriptGenerator() {
		this.dispose();
		genSql.getScriptGenerator().setVisible(true);
	}
	
	/**
	 * This method sends control to Database in order for the sql examples to be downloaded to a file
	 */
	public void downloadExamples() {
		db.downloadExamplesFile(examples);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource()==moreEx) {
			this.returnToExampleFinder();
		}
		else if(ae.getSource()==downloadEx) {
			this.downloadExamples();
		}
		else if(ae.getSource() == backToScriptGen) {
			this.returnToScriptGenerator();
		}
		else if(ae.getSource()==exit) {
			System.exit(0);
		}
	}
	
}
