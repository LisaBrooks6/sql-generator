import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

public class schemaTest {

	@Test
	public void testCreatingSchema() {
		// create a new instance of database 
		Database db = new Database();

		// set the name of the database
		db.setDbName("University");
		// set the number of tables
		db.setNoTables(3);

		// add a table - Student with 2 columns 
		db.addTable("Student", 3);
		// check that the test passes for each table being added 
		assertEquals("added", db.addColumnToTable("Student", "name", "varchar", 30));
		assertEquals("added", db.addColumnToTable("Student", "matric", "integer", null));
		assertEquals("added", db.addColumnToTable("Student", "advisor", "integer", null));

		// add the second table - Lecturer with 2 columns
		db.addTable("Lecturer", 2); 
		// add the columns 
		assertEquals("added", db.addColumnToTable("Lecturer", "name", "varchar", 30));
		assertEquals("added", db.addColumnToTable("Lecturer", "id", "Integer", null));

		// test adding a default value
		assertTrue(db.addConstraintsToColumn("Lecturer", "name", false, false, false, null, true, "Dr"));
		// test adding checks 
		ArrayList<Check> checks = new ArrayList<Check>();
		checks.add(0, new Check("<", "8"));
		// test that checks is not empty
		assertNotEquals(0, checks.size());

		assertTrue(db.addConstraintsToColumn("Lecturer", "id", true, true, true, checks, false, null));
		// set the primary key of each table
		db.getTable("Student").getColumn("matric").setPrimaryKey(true);
		// check that the primary key has been set for Student by checking the array list is of size 1
		assertEquals(1, db.getTable("Student").getPrimaryKey().size());

		db.getTable("Lecturer").getColumn("id").setPrimaryKey(true);
		// check that the primary key has been set for Lecturer, therefore check that the size of the array is 1
		assertEquals(1, db.getTable("Lecturer").getPrimaryKey().size());

		// set the foreign key 'advisor' in Student
		db.getTable("Student").getColumn("advisor").setForeignKey(new ForeignKey(db.getTable("Lecturer"), db.getTable("Lecturer").getColumn("id")));
		// check the foreign key is not empty
		ForeignKey fk = db.getTable("Student").getColumn("advisor").getForeignKey();
		assertNotNull(fk);
		assertEquals("Lecturer", fk.getTable().getName());
		assertEquals("id", fk.getColumn().getName());

		// add another table Course - 2 cols
		db.addTable("Course", 2);
		// add the columns 
		assertEquals("added", db.addColumnToTable("Course", "name", "varchar", 30));
		assertEquals("added", db.addColumnToTable("Course", "id", "Integer", null));

		// add another table - Enrolment with 3 columns
		db.addTable("Enrolment", 3);
		// add the columns 
		assertEquals("added", db.addColumnToTable("Enrolment", "student", "Integer", null));
		assertEquals("added", db.addColumnToTable("Enrolment", "course", "Integer", null));
		assertEquals("added", db.addColumnToTable("Enrolment", "gpa", "Integer", null));
		db.getTable("Enrolment").getColumn("student").setPrimaryKey(true);
		db.getTable("Enrolment").getColumn("course").setPrimaryKey(true);
		// check there is a composite primary key comprising of 2 columns
		assertEquals(2, db.getTable("Enrolment").getPrimaryKey().size());
		
		// set the foreign key 'advisor' in Student
		db.getTable("Enrolment").getColumn("student").setForeignKey(new ForeignKey(db.getTable("Student"), db.getTable("Student").getColumn("matric")));
		db.getTable("Enrolment").getColumn("course").setForeignKey(new ForeignKey(db.getTable("Course"), db.getTable("Course").getColumn("id")));

		// check the foreign key is not null
		assertNotNull(db.getTable("Enrolment").getColumn("course").getForeignKey());
		assertNotNull(db.getTable("Enrolment").getColumn("student").getForeignKey());
	}
}
