import java.util.ArrayList;

import org.chocosolver.solver.Solver;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
/**
 * Abstract class to specify the instance variables and methods signatures for 
 * all constraint solvers that will extend this abstract class
 */
public abstract class ConstraintSolver {
	// choco instance vars
	protected Solver solver;
	protected IntVar[] data;

	// normal instance vars
	protected String fileName;
	protected String type;
	protected ArrayList<Check> checkList;
	protected boolean unique;
	protected boolean notNull;
	protected int noData;
	
	// methods
	protected abstract void initialSetUp();
	protected abstract void readFile();
	protected abstract void addConstraints() throws ContradictionException;
	protected abstract boolean solve(); 
	protected abstract IntVar[] getData();
}
