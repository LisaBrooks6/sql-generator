import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.chocosolver.solver.exception.ContradictionException;

public class GeneratePopulationScript extends GUI implements ActionListener {
	private JLabel noExamplesLabel, description;
	private JButton back, backToPks, next, previousTable, genScript, editTableInfo;
	private JComboBox<Integer> noExamplesField;
	private JPanel buttonPanel, columnPanel, descriptionPanel;
	private ArrayList<JLabel> colName, typeOfData;
	private ArrayList<JTextField> colNameField;
	private ArrayList<JComboBox> typeOfDataCombo;

	private String[][] primaryKeyHolder;

	private ScriptGenerator scriptGenerator;
	private int indexOfRowInTable;
	private GeneratePopulationScript genPopScript;

	private String tableName;
	private int noCols;
	private boolean primaryKeyDataSetChoice = false;
	private boolean last = false;
	private final int MAX_NO_EX = 10;

	/**
	 * Constructor for GeneratePopulationScript
	 * @param scriptGenerator
	 * @param indexOfRowInTable
	 * @param db
	 */
	public GeneratePopulationScript(ScriptGenerator scriptGenerator, int indexOfRowInTable, Database db, GeneratePopulationScript genPopScript) {

		this.scriptGenerator = scriptGenerator;
		this.indexOfRowInTable = indexOfRowInTable;
		this.db = db;
		this.genPopScript = genPopScript;

		// set the table name and no cols instances to be used throughout the class
		if(indexOfRowInTable >= 0) {
			tableName = db.getTables().get(indexOfRowInTable).getName();
			noCols = db.getTable(tableName).getNoCols();
		}
		else{
			primaryKeyDataSetChoice = true;
		}

		this.setSize(900, 250);
		if(indexOfRowInTable == db.getTables().size()-1) {
			// equal to the last element in the array list 
			last = true;
		}
		this.initialSetUp();
	}

	@Override
	public void initialSetUp() {
		descriptionPanel = new JPanel();
		columnPanel = new JPanel(new GridLayout(noCols, 4));
		buttonPanel = new JPanel();	

		colName = new ArrayList<JLabel>();
		typeOfData = new ArrayList<JLabel>();
		colNameField = new ArrayList<JTextField>();
		typeOfDataCombo = new ArrayList<JComboBox>();
		primaryKeyHolder = new String [db.getNoTables()][db.getNoTables()];

		if(primaryKeyDataSetChoice){
			// set the title
			this.setTitle("Population Script Generation - Choose a data set for your primary keys");

			// add the description 
			description = new JLabel("Please choose a backend dictionary which best fits each column in order to populate sample data.");
			descriptionPanel.add(description);

			// this is when we are not yet dealing with individual tables
			for(int j = 0; j < db.getNoTables(); j++) {
				// get the primary key(s) of the current table
				ArrayList<String> primaryKeys = db.getTables().get(j).getPrimaryKey();
				ArrayList<String> clonedPrimaryKeys = db.getTables().get(j).getPrimaryKey();
				/*
				 * Loop through the arraylist of primary keys and if a primary key has a
				 * foreign key also then remove this primary key from list.
				 */
				for(int index = 0; index < clonedPrimaryKeys.size(); index++) {
					if(db.getTables().get(j).getColumn(clonedPrimaryKeys.get(index).trim()).getForeignKey() != null) {
						String removal = clonedPrimaryKeys.get(index);
						primaryKeys.remove(removal);
					}
				}

				for(int k = j; k < j+primaryKeys.size(); k++) {
					int primaryKeyNo = k-j;
					colName.add(k, new JLabel("Column: "));
					columnPanel.add(colName.get(k));

					colNameField.add(k, new JTextField(30));

					colNameField.get(k).setText(db.getTables().get(j).getName().trim()+"."+primaryKeys.get(primaryKeyNo)+" ("+db.getTables().get(j).getColumn(primaryKeys.get(primaryKeyNo).trim()).getType().toString().trim()+")");
					colNameField.get(k).setEnabled(false);
					columnPanel.add(colNameField.get(k));

					typeOfData.add(k, new JLabel("Choose type of data: "));
					columnPanel.add(typeOfData.get(k));

					typeOfDataCombo.add(k, new JComboBox());
					addTypesOfData(typeOfDataCombo.get(k), db.getTables().get(j).getColumn(primaryKeys.get(primaryKeyNo).trim()).getType().toString().trim());

					// add the primary key
					primaryKeyHolder[j][primaryKeyNo] = primaryKeys.get(primaryKeyNo);

					// if there is a saved data set set it as the chosen one
					populateSavedDataSets(typeOfDataCombo.get(k), k);
					columnPanel.add(typeOfDataCombo.get(k));
				}
			}
		}
		else{
			// set the title
			this.setTitle("Population Script Generation - Choose a data set for columns in "+tableName);
			// add the description 
			description = new JLabel("Please choose a backend dictionary which best fits each column in table "+tableName.toUpperCase()+" in order to populate sample data.");
			descriptionPanel.add(description);

			for(int j = 0; j < noCols; j++){ 
				colName.add(j, new JLabel("Column: "));
				columnPanel.add(colName.get(j));

				colNameField.add(j, new JTextField(30));
				colNameField.get(j).setText(tableName+"."+db.getTable(tableName).getColumns().get(j).getName()+" ("+db.getTable(tableName).getColumns().get(j).getType().toString().trim()+")");
				colNameField.get(j).setEnabled(false);
				columnPanel.add(colNameField.get(j));

				typeOfData.add(j, new JLabel("Choose type of data: "));
				columnPanel.add(typeOfData.get(j));

				typeOfDataCombo.add(j, new JComboBox());
				addTypesOfData(typeOfDataCombo.get(j), db.getTable(tableName).getColumns().get(j).getType().toString().trim());
				// if there is a saved data set set it as the chosen one
				populateSavedDataSets(typeOfDataCombo.get(j), j);
				columnPanel.add(typeOfDataCombo.get(j));
			}
		}
		// button initialisation
		back = new JButton("Back");
		back.addActionListener(this);
		backToPks = new JButton("Edit Primary Key Data Sets");
		backToPks.addActionListener(this);
		previousTable = new JButton("Previous Table");
		previousTable.addActionListener(this);
		next = new JButton("Choose relevant data for the next table");
		next.addActionListener(this);
		genScript = new JButton("Download Population Script");
		genScript.addActionListener(this);
		editTableInfo = new JButton("Edit Table Information");
		editTableInfo.addActionListener(this);

		if(indexOfRowInTable < 0) {
			// this means we are working on the first table and therefore there should be a back button
			buttonPanel.add(back);
			// next table button
			buttonPanel.add(next);
		}
		else if(indexOfRowInTable == 0 && !last) {
			// this means we are working on the first table and therefore there should be a back button
			buttonPanel.add(backToPks);
			// next table button
			buttonPanel.add(next);
		}
		else if(last){	
			// if we are on the last table then we can download the script
			noExamplesLabel = new JLabel("Data per table: ");
			noExamplesField = new JComboBox<Integer>();
			this.populateNoExamples(noExamplesField);
			noExamplesField.addActionListener(this);


			buttonPanel.add(noExamplesLabel);
			buttonPanel.add(noExamplesField);
			buttonPanel.add(previousTable);
			buttonPanel.add(genScript);	
		}
		else{
			// if we're not on the last table then we need to go to the next table
			buttonPanel.add(previousTable);
			buttonPanel.add(next);
		}

		this.add(descriptionPanel, BorderLayout.NORTH);
		this.add(columnPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Add to the combobox, the possible number of examples that can be chosen
	 * @param combo - the JComboBox to be populated
	 */
	public void populateNoExamples(JComboBox combo) {
		// start from 1 as we do not want 0 to be an option in the combobox
		for(int i = 1; i <= MAX_NO_EX; i++) {
			combo.addItem(i);
		}
		combo.setSelectedIndex(-1);
	}

	/**
	 * Populate the combobox with possible data files that will correspond to the 
	 * column being worked on.
	 * @param combo - the JComboBox to be populated
	 */
	public void addTypesOfData(JComboBox combo, String type) {
		if(type.equals("VarChar")) {
			combo.addItem("Names (concatenated first and surnames)");
			combo.addItem("Course Names");
			combo.addItem("First Names");
			combo.addItem("Surnames");
			combo.addItem("Addresses");
			combo.addItem("Contact Numbers");
			combo.addItem("Random String");
		}
		else if(type.equals("Integer")) {
			combo.addItem("Integers");
			combo.addItem("ID (8 length integer)");
			combo.addItem("Contact Numbers");
		}
		else if(type.equals("Date")) {
			combo.addItem("Dates of Birth");
		}
		else if(type.equals("Character")) {
			combo.addItem("Random Characters");
		}
		combo.setSelectedIndex(-1);
	}

	public void populateSavedDataSets(JComboBox combo, int i) {
		if(indexOfRowInTable < 0 && primaryKeyHolder != null) {
			for(int j = 0; j < primaryKeyHolder.length; j++) {
				// get the primary key(s) of the current table
				for(int k = 0; k < primaryKeyHolder[j].length; k++) {
					// to get the primary keys in that table (j) and then use the names of the columns
					if(primaryKeyHolder[j][k] != null && db.getTables().get(j).getColumn(primaryKeyHolder[j][k].trim()).getDataSet() != null){
						combo.setSelectedItem(db.getTables().get(j).getColumn(primaryKeyHolder[j][k].trim()).getDataSet().trim());
					}
				}
			}
		}
		else{
			if(db.getTables().get(indexOfRowInTable).getColumns().get(i).getDataSet() != null) {
				combo.setSelectedItem(db.getTables().get(indexOfRowInTable).getColumns().get(i).getDataSet().trim());

				if(db.getTables().get(indexOfRowInTable).getPrimaryKey().contains(db.getTables().get(indexOfRowInTable).getColumns().get(i).getName().trim())) {
					// if the column is a primary key then disable the combobox 
					combo.setEnabled(false);
				}
			}
			if(db.getTables().get(indexOfRowInTable).getColumns().get(i).getForeignKey() != null) {
				combo.setSelectedItem(db.getTables().get(indexOfRowInTable).getColumns().get(i).getForeignKey().getColumn().getDataSet().trim());
				combo.setEnabled(false);
			}
		}
	}

	/**
	 * This method checks that the entered information is captured and the screen for displaying the next table is visible
	 */
	public void goToNextTable(){
		if(this.captureEnteredInfo()) {
			// go to next table - this will not go out of bounds, as this feature is not available on the last screen (last table)
			GeneratePopulationScript genPop = new GeneratePopulationScript(scriptGenerator, indexOfRowInTable+1, db, this);
			this.dispose();
			genPop.setVisible(true);
		}
		else {
			System.out.println("AHHHH PROBLEM");
		}
	}

	/**
	 * This method returns the user to the previous GeneratePopulationScript screen and disposes the current one.
	 */
	public void goToPreviousTable() {
		genPopScript.setVisible(true);
		this.dispose();
	}

	/**
	 * This method disposes the current screen and returns the user to the Script Generator screen.
	 */
	public void returnToScriptGenerator() {
		scriptGenerator.setVisible(true);
		this.dispose();
	}

	/**
	 * This method will capture the user input and save the chosen data set to the relevant column
	 * @return result - boolean to determine whether the input was validated and saved or not.
	 */
	public boolean captureEnteredInfo() {
		boolean result = false;
		// capture the info entered 
		for(int i = 0; i < colNameField.size(); i++) {
			// loop through the screen and gather the entered data set
			if(typeOfDataCombo.get(i).getSelectedIndex() != -1){
				// then it has been selected
				String col = colNameField.get(i).getText().trim();
				String [] colSplit = col.split("[() \\.]+");
				/* the formatting is tableName.colName (colType), therefore the first index will be table name and the 
				 * second index (i.e 1) will be the col name
				 */
				String tName = colSplit[0].trim();
				String colName = colSplit[1].trim();

				String dataSetChosen = typeOfDataCombo.get(i).getSelectedItem().toString().trim();
				db.getTable(tName).getColumn(colName).setDataSet(dataSetChosen);
				result = true;
			}
			else{
				int colNum = i+1;
				JOptionPane.showMessageDialog(null, "Please choose a relevant data set for column "+colNum, "Error Message", JOptionPane.ERROR_MESSAGE);
				result = false;
			}
		}
		return result;
	}

	/**
	 * This method captures the user input by sending control to captureEnteredInfo() and then captures the number of examples
	 * the user wishes to produce for each table, this information is then sent across to Database to generate the population script.
	 * The current screen is disposed and the Script Generator screen is again made visible. If there is a problem with the entered number
	 * of tables, relevant JOptionPanes will be shown.
	 */
	public void downloadPopScript() {
		// get captured info for the last table
		this.captureEnteredInfo();
		// capture noExamples required and generate the script using the entered info
		if(noExamplesField.getSelectedIndex() >= 0) {
			int noExamples = Integer.parseInt(noExamplesField.getSelectedItem().toString());
			try{
				db.generatePopulationScript(noExamples, this);
				// dispose this screen and make visible the script generator
				this.dispose();
				scriptGenerator.setVisible(true);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null, "There is no data to support your check. The data in the chosen file (fileName) is in the range (minVal, maxVal)."
						+ "\nPlease change your check and try again. Remember to download your updated create script!", "Contradiction Exception Error in Data", JOptionPane.ERROR_MESSAGE);

				// Add a button to allow the user to easily return to edit the checks.
				buttonPanel.add(editTableInfo);
				this.setVisible(true);
			}
		}
		else {
			JOptionPane.showMessageDialog(null, "Please enter a valid number for number of examples per table", "Error Message", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void returnToTableInfoScreen() {
		scriptGenerator.getTInfo().setVisible(true);
		this.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == next){
			this.goToNextTable();
		}
		else if(ae.getSource() == back) {
			this.returnToScriptGenerator();
		}
		else if(ae.getSource() == previousTable || ae.getSource() == backToPks) {
			this.goToPreviousTable();
		}
		else if(ae.getSource() == genScript) {
			this.downloadPopScript();
		}
		else if(ae.getSource() == editTableInfo) {
			this.returnToTableInfoScreen();
		}
	}
}
