import java.util.ArrayList;

public abstract class ConstraintValidator {
	// instance vars
	protected ArrayList<Check> checkList;
	protected boolean notNull;
	protected boolean unique;
	protected int noData;
	
	// methods
	protected abstract boolean validate();
}
