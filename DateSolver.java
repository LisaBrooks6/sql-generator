import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import org.chocosolver.solver.*;
import org.chocosolver.solver.variables.*;
import org.chocosolver.solver.constraints.*;
import org.chocosolver.solver.search.strategy.*;
import org.chocosolver.solver.exception.ContradictionException;


/*
 * The aim of this class is to deal with the constraints on columns with only 
 * primary keys, and columns with no key constraints at all. This class will only 
 * work for integers.
 */

public class DateSolver extends ConstraintSolver {
	private ArrayList<String> fileContent = new ArrayList<String>(); 
	private Map<Integer, ArrayList<String>> correspondingDates = new HashMap<Integer, ArrayList<String>>();

	/**
	 * Constructor for DateSolver
	 * @param fileName
	 * @param checkList
	 * @param unique
	 * @param notNull
	 * @param type
	 * @param noData
	 */
	public DateSolver(String fileName, ArrayList<Check> checkList, boolean unique, boolean notNull, String type, int noData) {
		this.fileName = fileName;
		this.checkList = checkList;
		this.type = type;
		this.noData = noData;
		this.unique = unique;
		this.notNull = notNull;

		solver = new Solver("date_constraint_solver");
		this.initialSetUp();
	}

	/**
	 * Method to set up the IntVar array.
	 */
	public void initialSetUp() {
		this.readFile();
		// move the lengths of the strings in fileContent into an int array 
		int [] content = new int[fileContent.size()];
		int i = 0;
		for(String input : fileContent) {
			// load the array position with the file content
			// get the year
			Integer year = this.getYear(input.trim());
			if(year == null) {
				// set the key as 0 if the value is null
				content[i] = 0;
			}
			else {
				content[i] = year;
			}
			// add the full year dd/mm/yyyy to the fileContent arraylist
			ArrayList<String> vals;
			if(correspondingDates.containsKey(content[i])){
				// get the arraylist of values
				vals = correspondingDates.get(content[i]);
			}
			else {
				vals = new ArrayList<String>();
			}
			vals.add(input.trim());
			// update the values
			correspondingDates.put(content[i], vals);
			// increment
			i++;
		}

		/* 
		 * the data array is of length noData. Each value 
		 * has domain of all values in content.
		 */
		data = VF.enumeratedArray("data", noData, content, solver);

		try {
			this.addConstraints();
		} 
		catch (ContradictionException e) {
			e.printStackTrace();
		}
	}

	public Integer getYear(String dateString) {
		if(dateString.equals("null") && !notNull) {
			return null;
		}
		else {
			// the string will come in as day/month/year
			String[] dateParts = dateString.split("[ /]+");
			// day and month will be the first two positions in the array, year will be at pos 2
			Integer year = Integer.parseInt(dateParts[2]);
			return year;
		}
	}

	/**
	 * Method to add the constraints that the data IntVar array must satisfy.
	 */
	public void addConstraints() throws ContradictionException {
		// check constraint
		for(int i = 0; i < data.length; i++) {
			if(checkList != null) {
				for(Check c : checkList) {
					String dateVal = c.getValue().trim();
					int year = Integer.parseInt(dateVal);
					// post a constraint to check the year is valid
					solver.post(ICF.arithm(data[i], c.getCheckOperator().trim(), year));
				}
			}
		}

		// heuristics to ensure the data is a random value each time
		solver.set(ISF.random_value(data));
		// propagate
		try {
			solver.propagate();
		}
		catch(ContradictionException e) {
			throw new ContradictionException();
		}
	}

	/**
	 * Method to read the input file.
	 */
	public void readFile() {
		// loop through the file and find the min and max values
		try {
			FileReader reader = new FileReader(fileName);
			Scanner sc = new Scanner(reader);
			while(sc.hasNextLine()) {
				String content = sc.nextLine();
				if(notNull) {
					if(!content.trim().equals("null")) {
						fileContent.add(content.trim());
					}
				}
				else {
					// remove all whitespace from content
					fileContent.add(content.trim());
				}
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("The file "+fileName+" was not found");
		}
	}

	/**
	 * Method to return the outcome of whether a solution was found or not
	 * @return true/false 
	 */
	public boolean solve() {
		return solver.findSolution();
	}

	/**
	 * Method to return the solution which satisfies the constraints
	 * @return data
	 */
	public IntVar[] getData() {
		return data;
	}

	public ArrayList<String> getDateData() {
		ArrayList<String> dates = new ArrayList<String>();
		while(dates.size() < data.length){
			for(IntVar s : data) {
				// get the string value of the IntVar
				String date = String.valueOf(s.getValue());

				// get the dates which correspond to the solution found
				if(date.equals("null") && !notNull) {
					// if the solution is null and null is an allowed value, then add it to dates
					dates.add("null");
				}
				else {
					ArrayList<String> vals = correspondingDates.get(Integer.parseInt(date.trim()));
					// get a random int for the position
					Random r = new Random();
					int rand = r.nextInt(vals.size());
					// attempt to add the value at position rand to dates
					if(unique) {
						if(!dates.contains(vals.get(rand).trim())) {
							// the value is not in the arraylist, therefore add it
							dates.add(vals.get(rand).trim());
						}
					}
					else {
						// don't need to check if it already is in the arraylist as it does not need to be unique
						dates.add(vals.get(rand).trim());
					}
				}
			}
		}
		return dates;
	}
}
