import java.util.ArrayList;

import org.chocosolver.solver.variables.IntVar;

public class IntegerValidator extends ConstraintValidator {
	IntVar[] solution;
	
	public IntegerValidator(IntVar[] solution, ArrayList<Check> checkList, boolean notNull, boolean unique, int noData) {
		this.solution = solution;
		this.checkList = checkList;
		this.notNull = notNull;
		this.unique = unique;
		this.noData = noData;
	}
	
	@Override
	protected boolean validate() {
		boolean pass = true;
		if(solution.length != noData) {
			System.out.println("Integer failed as solution is not equal to noData");
			pass = false;
		}
		if(checkList!=null) {
			for(Check c: checkList) {
				String op = c.getCheckOperator();
				Integer val = Integer.parseInt(c.getValue());
				if(checkValidator(op, val) == false) {
					System.out.println("Integer failed for check: "+op+" "+val);
					pass = false;
				}
			}
		}
		if(notNull) {
			for(IntVar i : solution) {
				if(i == null) {
					System.out.println("Integer failed for null value");
					pass = false;
				}
			}
		}
		if(unique) {
			ArrayList<Integer> uniqueValidation = new ArrayList<Integer>();
			for(IntVar i: solution) {
				if(!uniqueValidation.contains(i)) {
					uniqueValidation.add(i.getValue());
				}
				else{
					// this already exists in the arraylist, hence not unique
					System.out.println("Integer failed as duplicate was found");
					pass = false;
				}
			}
		}
		return pass;
	}

	public boolean checkValidator(String op, Integer val) {
		if(op.equals("<")) {
			for(IntVar i: solution) {
				if(!(i.getValue() < val)){
					return false;
				}
			}
		}
		else if(op.equals(">")) {
			for(IntVar i: solution) {
				if(!(i.getValue() > val)){
					return false;
				}
			}
		}
		else if(op.equals("=")) {
			for(IntVar i: solution) {
				if(i.getValue() != val){
					return false;
				}
			}
		}
		else if(op.equals("!=")) {
			for(IntVar i: solution) {
				if(i.getValue() == val){
					return false;
				}
			}
		}
		else if(op.equals("<=")) {
			for(IntVar i: solution) {
				if(i.getValue() > val){
					return false;
				}
			}
		}
		else if(op.equals(">=")) {
			for(IntVar i: solution) {
				if(i.getValue() < val){
					return false;
				}
			}
		}
		return true;
	}

}
