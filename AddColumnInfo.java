import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddColumnInfo extends GUI implements ActionListener, ItemListener {
	private String tableName;
	private int noCols;
	private AddTableInfo tableInfoScreen;
	private int indexOfRow;

	private boolean accessed = false;

	private JButton back, save;
	private ArrayList<JLabel> nameLabel, typeLabel;
	private ArrayList<JTextField> nameField;
	private ArrayList<JComboBox> typeComboBox;
	private ArrayList<JLabel> sizeLabel;
	private ArrayList<JTextField> sizeField;
	private ArrayList<JButton> addConstraints;
	// new 
	private ArrayList<JLabel> pkLabel;
	private ArrayList<JCheckBox> primaryKeyCheck;

	private JPanel columnInfoPanel, buttonPanel;

	/**
	 * Constructor for AddColumnInfo
	 * @param tableName String holding table name
	 * @param noCols int holding the number of columns
	 * @param tableInfoScreen instance of AddTableInfo
	 * @param indexOfRow int determining the index of the row currently being worked on
	 * @param db instance of the Database containing the schema 
	 */
	public AddColumnInfo(String tableName, int noCols, AddTableInfo tableInfoScreen, int indexOfRow, Database db) {
		// allow this screen to close without closing the full system
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("Schema Generation : Add column information for "+tableName);
		this.tableName = tableName;
		this.noCols = noCols;
		this.tableInfoScreen = tableInfoScreen;
		this.indexOfRow = indexOfRow;
		this.db = db;


		// initial set up has not been accessed previously therefore boolean = false
		this.initialSetUp();
	}

	public void initialSetUp() {

		if(noCols == 1){
			this.setSize(1100, 100);
		}
		else{
			int height = 70*noCols;
			this.setSize(1100, height);
		}

		if(!accessed){
			// this is the first time initialSetUp has been entered with this instance

			// initiate JComponents
			back = new JButton("Back");
			save = new JButton("Save Column Information");
			addConstraints = new ArrayList<JButton>();

			nameLabel = new ArrayList<JLabel>();
			typeLabel = new ArrayList<JLabel>();
			sizeLabel = new ArrayList<JLabel>();

			nameField = new ArrayList<JTextField>();
			typeComboBox = new ArrayList<JComboBox>();
			sizeField = new ArrayList<JTextField>();

			pkLabel = new ArrayList<JLabel>();
			primaryKeyCheck = new ArrayList<JCheckBox>();

			columnInfoPanel = new JPanel(new GridLayout(noCols, 9));

			buttonPanel = new JPanel();

			// create all the components for the number of columns required
			for(int j = 0; j < noCols; j++){ 
				addComponents(j);
			}

			// add back to the buttonPanel
			buttonPanel.add(back);
			buttonPanel.add(save);

			// add action listeners to buttons back and complete 
			back.addActionListener(this);
			save.addActionListener(this);

			// add the panels columnInfoPanel and buttonPanel to the main JPanel
			this.add(columnInfoPanel, BorderLayout.CENTER);
			this.add(buttonPanel, BorderLayout.SOUTH);
		}

		else{
			// this is when the GUI has already been created, we want to add additional cols
			int previousNoCols = db.getTable(tableName).getNoCols();
			for(int i = 0; i < previousNoCols; i++) {
				if(db.getTable(tableName).getColumns().size() > previousNoCols && db.getTable(tableName).getColumns().get(i) != null){
					nameField.get(i).setText(db.getTable(tableName).getColumns().get(i).getName());
					typeComboBox.get(i).setSelectedItem(db.getTable(tableName).getColumns().get(i).getType());
				}
			}
			columnInfoPanel.setLayout(new GridLayout(noCols, 7));

			for(int j = nameLabel.size(); j < noCols; j++) {
				addComponents(j);
			}
		}
	}

	/**
	 * This method adds JComponents at position j in the arraylists (holding the JComponents)
	 * @param j - position in the arraylist that the component should be added 
	 */
	public void addComponents(int j){
		nameLabel.add(j, new JLabel("Column Name: "));
		columnInfoPanel.add(nameLabel.get(j));

		nameField.add(j, new JTextField(30));
		columnInfoPanel.add(nameField.get(j));

		typeLabel.add(j, new JLabel("Column Type: "));
		columnInfoPanel.add(typeLabel.get(j));

		typeComboBox.add(j, new JComboBox());
		addTypeItems(typeComboBox.get(j));
		columnInfoPanel.add(typeComboBox.get(j));
		typeComboBox.get(j).addActionListener(this);
		typeComboBox.get(j).addItemListener(this);

		sizeLabel.add(j, new JLabel("		Size: "));
		columnInfoPanel.add(sizeLabel.get(j));

		sizeField.add(j, new JTextField(10));
		columnInfoPanel.add(sizeField.get(j));
		// make it not enabled until the type is chosen
		sizeField.get(j).setEnabled(false);

		pkLabel.add(j, new JLabel("	Primary Key: "));
		columnInfoPanel.add(pkLabel.get(j));

		primaryKeyCheck.add(j, new JCheckBox());
		columnInfoPanel.add(primaryKeyCheck.get(j));
		primaryKeyCheck.get(j).addActionListener(this);

		addConstraints.add(j, new JButton("Add Constraints"));
		columnInfoPanel.add(addConstraints.get(j));
		// add action listeners to Add Details buttons 
		addConstraints.get(j).addActionListener(this);
	}

	/**
	 * Mutator for accessed
	 * @param a - this is a boolean to determine whether the instance has been
	 * previously accessed
	 */
	public void setAccessed(boolean a) {
		this.accessed = a;
	}

	/**
	 * This method will be entered if the tableName already exists. This will
	 * only happen when the AddColumnInfo has been entered before for this tableName
	 * thus meaning we dont need to populate the instance variables. Database is passed
	 * in as this may have changed slightly since the instance of AddColumnInfo was accessed. 
	 * @param noCols - int containing the updated number of columns
	 * @param db - instance of Database containing the schema
	 */
	public void updateColInfo(int noCols, Database db) {
		// this is the updated info
		this.noCols = noCols;
		this.db = db;

		// set the new number of columns for the table
		db.getTable(tableName).setNoCols(noCols);

		// now want to go to initial set up again
		// boolean accessed will be true here as we have accessed the initial set up before
		this.setAccessed(true);
		this.initialSetUp(); 
	}

	/**
	 * This method adds the possible column types to the combobox
	 * @param typeComboBox - the JCombBox to be populated
	 */
	public void addTypeItems(JComboBox typeComboBox){
		typeComboBox.addItem("VarChar");
		typeComboBox.addItem("Character");
		typeComboBox.addItem("Integer");
		typeComboBox.addItem("Date");

		typeComboBox.setSelectedIndex(-1);
	}

	/**
	 * This method is entered when the user presses 'Done'. Control is passed to a helper 
	 * method to validate and save the user input.
	 */
	public void afterSave() {
		// check if the columns are already created - if not then create them
		this.captureColumnInformation();
	}

	/**
	 * This method validates and captures the user input. If the columns are already created then the 
	 * instances are updated, else they are created.
	 */
	public void captureColumnInformation() {
		ArrayList<Boolean> sizeValidated = new ArrayList<Boolean>();
		ArrayList<Boolean> nameErrors = new ArrayList<Boolean>();
		ArrayList<Boolean> typeErrors = new ArrayList<Boolean>();
		ArrayList<Boolean> primaryKeyChoices = new ArrayList<Boolean>();

		for(int i = 0; i < noCols; i++) {
			/* refresh the array lists. We do not need to refresh the primary key array list 
			 * because we want to evaluate this over all columns.
			 */
			sizeValidated = new ArrayList<Boolean>();
			nameErrors = new ArrayList<Boolean>();
			typeErrors = new ArrayList<Boolean>();
			
			int colNumber = i+1;
			if(nameField.get(i).getText().isEmpty() || typeComboBox.get(i).getSelectedItem() == null) {
				if(nameField.get(i).getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Please enter the name of column "+colNumber, "Error with column name", JOptionPane.ERROR_MESSAGE);
					nameErrors.add(false);
				}
				if(typeComboBox.get(i).getSelectedIndex() < 0) {
					JOptionPane.showMessageDialog(null, "Please choose a type for column "+colNumber, "Error with column type", JOptionPane.ERROR_MESSAGE);
					typeErrors.add(false);
				}
			}
			else {
				if(!db.getTable(tableName).columnExistance(nameField.get(i).getText().trim())){
					// get the information entered
					String name = nameField.get(i).getText().trim();
					String type = typeComboBox.get(i).getSelectedItem().toString();

					if(sizeField.get(i).isEnabled()){
						if(sizeValidator(name, sizeField.get(i).getText().trim())){
							Integer size = Integer.parseInt(sizeField.get(i).getText().trim());
							db.addColumnToTable(tableName, name, type, size);
							if(primaryKeyCheck.get(i).isSelected()) {
								// set the primary key
								db.getTable(tableName).getColumn(nameField.get(i).getText().trim()).setPrimaryKey(true);
								primaryKeyChoices.add(true);
							}
							else{
								primaryKeyChoices.add(false);
							}
							// disable the field so that the name cannot be edited
							nameField.get(i).setEnabled(false);
							sizeValidated.add(true);
						}
						else {
							// if the sizeEntered is not a number flash up a warning and clear the field.
							sizeField.get(i).setText("");
							sizeValidated.add(false);
						}
					}
					else{
						// size is null
						db.addColumnToTable(tableName, name, type, null);
						if(primaryKeyCheck.get(i).isSelected()) {
							// set the new primary key
							db.getTable(tableName).getColumn(nameField.get(i).getText().trim()).setPrimaryKey(true);
							primaryKeyChoices.add(true);
						}
						else{
							primaryKeyChoices.add(false);
						}
						// disable the field so that the name cannot be edited
						nameField.get(i).setEnabled(false);
					}
				}
				else{
					if(primaryKeyCheck.get(i).isSelected()) {
						// set the primary key
						db.getTable(tableName).getColumn(nameField.get(i).getText().trim()).setPrimaryKey(true);
						primaryKeyChoices.add(true);
					}
					else{
						primaryKeyChoices.add(false);
					}
				}
			}
		}

		if(!sizeValidated.contains(false) && !nameErrors.contains(false) && !typeErrors.contains(false) && primaryKeyChoices.contains(true)){
			tableInfoScreen.refreshAfterButtonReturn(indexOfRow, this);
			this.dispose();
		}
		else{
			if(!primaryKeyChoices.contains(true) && db.getTable(tableName).getPrimaryKey().isEmpty()){
				JOptionPane.showMessageDialog(null, "Please choose the primary key of the table", "Error Message", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * This method captures the information entered for a column - this is entered when the user presses to add constraints 
	 * to a specific column. At this moment the column has not yet been created, this method creates it.
	 * @param colIndex - int containing the index in the array list of the 'Add Column Info' button that was pressed
	 */
	public void captureSpecificColumnInformation(int colIndex) {
		// raise JOptionPanes when either one or both of name and type have not been entered
		if(nameField.get(colIndex).getText().isEmpty() || typeComboBox.get(colIndex).getSelectedItem() == null) {
			if(nameField.get(colIndex).getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Please enter the name of the column", "Error with column name", JOptionPane.ERROR_MESSAGE);
			}
			if(typeComboBox.get(colIndex).getSelectedIndex() < 0) {
				JOptionPane.showMessageDialog(null, "Please choose a type for your column", "Error with column type", JOptionPane.ERROR_MESSAGE);
			}
		}
		// else if the name and type have both been entered continue to capture this data from the interface
		else if(!nameField.get(colIndex).getText().isEmpty() && typeComboBox.get(colIndex).getSelectedItem() != null){
			// get the information entered
			String name = nameField.get(colIndex).getText().trim();
			String type = typeComboBox.get(colIndex).getSelectedItem().toString();
			Integer size = null;
			/* if the size is required it will be enabled, therefore we will check if the entered value is a number and if so save this as size.
			 * Else raise the error in a JOptionPane and the user will need to re-enter this information before continuing.
			 */
			if(sizeField.get(colIndex).isEnabled()){
				if(sizeValidator(name, sizeField.get(colIndex).getText().trim())){
					// size is ok therefore move on to add the column to the table
					db.addColumnToTable(tableName, name, type, size);
					// disable the field so that the name cannot be edited
					nameField.get(colIndex).setEnabled(false);
					// if the primary key is checked, set the column as unique and not null
					if(primaryKeyCheck.get(colIndex).isSelected()) {
						db.getTable(tableName).getColumn(nameField.get(colIndex).getText().trim()).setPrimaryKey(true);
					}
					AddColumnConstraints constraints = new AddColumnConstraints(tableName, name, type, size, this, colIndex, db, primaryKeyCheck.get(colIndex).isSelected());
					constraints.setVisible(true);
					this.dispose();
				}
				else{
					sizeField.get(colIndex).setText("");
				}
			}
			else{
				// create the column, as the column is being created here, there are no constraints
				db.addColumnToTable(tableName, name, type, size);
				// disable the field so that the name cannot be edited
				nameField.get(colIndex).setEnabled(false);
				// if the primary key is checked, set the column as unique and not null
				if(primaryKeyCheck.get(colIndex).isSelected()) {
					db.getTable(tableName).getColumn(nameField.get(colIndex).getText().trim()).setPrimaryKey(true);
				}
				AddColumnConstraints constraints = new AddColumnConstraints(tableName, name, type, size, this, colIndex, db, primaryKeyCheck.get(colIndex).isSelected());
				constraints.setVisible(true);
				this.dispose();
			}
		}
	}

	/**
	 * This method validates the entered size by checking that it is an integer, and also 
	 * that it is an integer which is not negative.
	 * @param name
	 * @param enteredSize
	 * @return
	 */
	public boolean sizeValidator(String name, String enteredSize) {
		try{
			Integer size = Integer.parseInt(enteredSize);
			if(size < 0) {
				// can't be a negative number
				JOptionPane.showMessageDialog(null, "Please enter a valid positive integer for size of column "+name, "Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			else{
				return true;
			}
		}
		catch(NumberFormatException e){
			// if the sizeEntered is not a number flash up a warning and clear the field.
			JOptionPane.showMessageDialog(null, "Please enter a valid integer for size of column "+name, "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	/** 
	 * This method makes this screen visible. This is used when the user chooses to return to this page 
	 * when it has been previously entered.
	 */
	public void refresh(){
		this.setVisible(true);
	}

	/** 
	 * This method sets the label of the button after the constraints have been set.
	 * @param i - int containing the index in the arraylist of the button which was pressed
	 */
	public void setConstraintsButton(int i) {
		addConstraints.get(i).setText("Edit Constraints");
		this.refresh();
	}

	/**
	 * This method disposes the current screen and returns the user to the Table Info screen.
	 */
	public void returnToPreviousScreen() {
		tableInfoScreen.refreshAfterPreviousPageRequest(indexOfRow, this);
		this.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == save){	
			this.afterSave();
		}
		else if(ae.getSource() == back) {
			this.returnToPreviousScreen();
		}
		for(int j = 0; j < noCols; j++){
			if(ae.getSource() == addConstraints.get(j)) {
				// add column
				captureSpecificColumnInformation(j);
			}
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		for(int i = 0; i < noCols; i++){
			if(e.getItemSelectable() == typeComboBox.get(i)){
				String selected = e.getItem().toString();
				if(selected.equals("VarChar") || selected.equals("Character")) {
					sizeField.get(i).setEnabled(true);
				}
				else {
					sizeField.get(i).setEnabled(false);
				}
			}
		}
	}

}
