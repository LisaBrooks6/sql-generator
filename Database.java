import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;

public class Database {
	private String dbName;
	private int noTables;
	private ArrayList<Table> tables = new ArrayList<Table>();
	private boolean populatedPopulationScript = false;
	private ChosenSqlKeywords chosen = null;
	private ArrayList<ArrayList<String>> pkData;
	private ArrayList<ArrayList<String>> fkData;
	private ArrayList<ArrayList<String>> otherColumnData;
	private GeneratePopulationScript genPopScreen = null;

	/**
	 * Constructor for Database - this will not be used in this implementation but is included
	 * for flexibility
	 * @param name
	 * @param noTables
	 */
	public Database(String name, int noTables) {
		this.dbName = name;
		this.noTables = noTables;
	}

	/** 
	 * the basic constructor for Database 
	 */
	public Database() {}

	/**
	 * Accessor for dbName
	 * @return dbName;
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * Mutator for dbName
	 * @param dbName
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * Accessor for noTables
	 * @return noTables
	 */
	public int getNoTables() {
		return noTables;
	}

	/**
	 * Mutator for noTables
	 * @param noTables
	 */
	public void setNoTables(int noTables) {
		this.noTables = noTables;
	}

	/**
	 * Accessor for tables
	 * @return tables
	 */
	public ArrayList<Table> getTables() {
		return tables;
	}

	/**
	 * Mutator for tables
	 * @param tables
	 */
	public void setTables(ArrayList<Table> tables){
		this.tables = tables;
	}

	/**
	 * Accessor for pkData 
	 * @return pkData
	 */
	public ArrayList<ArrayList<String>> getPkData() {
		return pkData;
	}

	/**
	 * Mutator for pkData 
	 * @param pkData
	 */
	public void setPkData(ArrayList<ArrayList<String>> pkData) {
		this.pkData = pkData;
	}

	/**
	 * Accessor for fkData
	 * @return fkData
	 */
	public ArrayList<ArrayList<String>> getFkData() {
		return fkData;
	}

	/**
	 * Mutator for fkData
	 * @param fkData
	 */
	public void setFkData(ArrayList<ArrayList<String>> fkData) {
		this.fkData = fkData;
	}

	/**
	 * Accessor for otherColumnData
	 * @return otherColumnData
	 */
	public ArrayList<ArrayList<String>> getOtherColumnData() {
		return otherColumnData;
	}

	/**
	 * Mutator for otherColumnData
	 * @param otherColumnData
	 */
	public void setOtherColumnData(ArrayList<ArrayList<String>> otherColumnData) {
		this.otherColumnData = otherColumnData;
	}

	/**
	 * This method creates a new instance of table and adds it to the arraylist tables
	 * @param tableName
	 * @param noCols
	 */
	public void addTable(String tableName, int noCols){
		Table t = new Table(tableName, noCols);
		tables.add(t);
	}

	/**
	 * This method adds a new column to the specified table, if the String returned is empty then the add was not 
	 * successful, if it is "added" then the table adding was successful.
	 * @param tableName
	 * @param colName
	 * @param colType
	 * @param size
	 * @return outcome
	 */
	public String addColumnToTable(String tableName, String colName, Object colType, Integer size){//, boolean notNull, boolean unique, boolean check, ArrayList<String> checkOps, ArrayList<String> checkVals, boolean defaultCheck, String defaultVal) {
		String outcome = "";
		if(getTable(tableName) != null){
			outcome = getTable(tableName).addColumn(colName, colType, size);
		}
		return outcome;
	}

	/**
	 * This method adds the constraints to the specified column and returns true or false dependent on if the constraints 
	 * were added successfully.
	 * @param tableName
	 * @param colName
	 * @param notNull
	 * @param unique
	 * @param check
	 * @param checkList
	 * @param defaultCheck
	 * @param defaultVal
	 * @return true/false
	 */
	public boolean addConstraintsToColumn(String tableName, String colName, boolean notNull, boolean unique, boolean check, ArrayList<Check> checkList, boolean defaultCheck, String defaultVal) {
		return getTable(tableName).addConstraintsToColumn(colName, notNull, unique, check, checkList, defaultCheck, defaultVal);
	}

	/**
	 * Method to return the Table instance corresponding to the name of the table.
	 * @param tableName
	 * @return table instance or null if no table exist
	 */
	public Table getTable(String tableName) {
		// get the specific table using the table name
		for(int i = 0; i < tables.size(); i++){
			if(tables.get(i).getName().equals(tableName)){
				return tables.get(i);
			}
		}
		return null;
	}

	/**
	 * This method will remove the table at index from the arraylist tables.
	 * @param index the index of the table to be removed from tables
	 */
	public void removeTable(int index) {
		tables.remove(index);
	}

	/**
	 * This method returns the index of the specific Table (corresponding to the table name)
	 * in tables
	 * @param tableName
	 * @return i - the index of the table or null if the table does not exist
	 */
	public Integer getTableIndex(String tableName) {
		for(int i = 0; i < tables.size(); i++) {
			if(tables.get(i).getName().equals(tableName)){
				return i;
			}
		}
		return null;
	}

	/**
	 * This method returns the number of primary keys in the database
	 * @return noPks int holding the number of primary keys in the db 
	 */
	public int getNoPks() {
		int noPks = 0;
		for(Table t:tables) {
			noPks += t.getPrimaryKey().size();
		}
		return noPks;
	}

	/**
	 * This method generates the create script using the entered schema and prints to a file 
	 * named 'createScript.txt'
	 */
	public void downloadCreateScript() {
		// PrintWriter stuff
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new File("createScript.txt"));
			// store all foreign keys in the database in the arraylist
			ArrayList<String> foreignKey = new ArrayList<String>();
			// store all the checks in the database in the arraylist
			ArrayList<String> checks = new ArrayList<String>();

			// create the database name 
			writer.println("CREATE DATABASE "+dbName+";");
			writer.println();
			for(int i = 0; i < tables.size(); i++){
				// new checks, foreignKeys, and primaryKey for each table
				ArrayList<String> primaryKey = new ArrayList<String>();

				Table t = tables.get(i);
				writer.println("CREATE TABLE "+t.getName());
				writer.println("(");
				for(int j = 0; j < t.getNoCols(); j++){
					String printLine = t.getColumns().get(j).getName()+" "+t.getColumns().get(j).getType().toString();
					if(t.getColumns().get(j).getSize() != null){
						printLine += "("+t.getColumns().get(j).getSize()+")";
					}
					// not null check
					if(t.getColumns().get(j).isNotNull()){
						printLine += " NOT NULL";
					}
					if(t.getColumns().get(j).isUnique()){
						printLine += " UNIQUE";
					}
					if(t.getColumns().get(j).isDefaultCheck()){
						printLine += " DEFAULT '"+t.getColumns().get(j).getDefaultVal()+"'";
					}
					if(t.getColumns().get(j).isCheck()){
						for(int k = 0; k < t.getColumns().get(j).getCheckList().size(); k++){
							String check = "ALTER TABLE "+t.getName()+" ADD CONSTRAINT check_"+t.getName()+"_"+t.getColumns().get(j).getName()+"_"+k+" CHECK (";
							String type = t.getColumns().get(j).getType().toString().trim();
							if(type.equals("VarChar") || type.equals("Character")) {
								check += "length("+t.getColumns().get(j).getName()+") ";
							}
							else if(type.equals("Integer") || type.equals("Date")){
								check += t.getColumns().get(j).getName() + " ";
							}
							check += t.getColumns().get(j).getCheckList().get(k).getCheckOperator()+" ";
							check += t.getColumns().get(j).getCheckList().get(k).getValue();
							check += ");";
							checks.add(check);
						}
					}
					if(t.getColumns().get(j).isPrimaryKey()){
						// there could be multiple primary keys involved in a composite primary key 
						primaryKey.add(t.getColumns().get(j).getName().trim());
					}
					if(t.getColumns().get(j).getForeignKey()!= null) {
						ForeignKey fk = t.getColumns().get(j).getForeignKey();
						foreignKey.add("ALTER TABLE "+t.getName()+" ADD CONSTRAINT fk_"+t.getName().trim()+"_"+j+" FOREIGN KEY ("+t.getColumns().get(j).getName()+") REFERENCES "+fk.getTable().getName()+"("+fk.getColumn().getName()+");");
					}
					printLine += ",";
					writer.println(printLine);
				}

				/* primarykey is always going to be of size > 0 due to checks previously to ensure there 
				 * is a primary key for each table. However, in the unlikely case where the other checks 
				 * are removed this will be a useful check.
				 */
				if(primaryKey.size() > 0){
					// we have a composite primary key and therefore should have a separate statement
					String pkString = "CONSTRAINT pk_"+t.getName().trim()+"_"+i+" PRIMARY KEY (";
					for(String s : primaryKey) {
						pkString += s+", ";
					}
					// cut off the end of the string i.e the ', ' as this is not required at the end
					String pkStringEdited = pkString.substring(0, pkString.length()-2);
					pkStringEdited += ")";
					writer.println(pkStringEdited);
				}
				writer.println(");");
				writer.println();
			}

			if(checks.size() > 0){
				for(int k = 0; k < checks.size(); k++) {
					writer.println(checks.get(k).trim());
				}
			}

			if(foreignKey.size() != 0){
				for(int m = 0; m < foreignKey.size(); m++){
					writer.println(foreignKey.get(m));
				}
			}
			writer.flush();
			JOptionPane.showMessageDialog(null, "Create Script generated, saved in the same directory under 'createScript.txt'", "Information Message", JOptionPane.INFORMATION_MESSAGE);
		} 
		catch(FileNotFoundException e) {
			System.out.println("createScript.txt file not found!");
		}
		finally{
			writer.close();
		}
	}

	/**
	 * Method to return the position of the primary key at tableName.primaryKeyColName
	 * in the ArrayList pkData.
	 * @param tableName the String holding the name of the table
	 * @param primaryKeyColName the String holding the name of the column
	 * @return an Integer holding the position in pkData, else null if not found
	 */
	public Integer getPosInPKArrayList(String tableName, String primaryKeyColName) {
		int noPks = 0;
		Integer posInArrayList = null;
		for(int i = 0; i < getNoTables(); i++) {
			// looping through the number of tables

			// get the ArrayList holding the primary keys of that table
			ArrayList<String> pksInTable = getTables().get(i).getPrimaryKey();
			if(getTables().get(i).getName().trim().equals(tableName)) {
				// if we are at the table that the primary key is in
				if(pksInTable.size()==1) {
					// there is only one primary key in the table, therefore the one we want
					noPks++;
					posInArrayList = noPks-1;
					return posInArrayList;
				}
				else {
					// there is more than 1 primary key in the table
					for(String s : pksInTable) {
						if(s.trim().equals(primaryKeyColName)) {
							noPks++;
							posInArrayList = noPks-1;
							return posInArrayList;
						}
						else{
							// increment to the next pk
							noPks++;
						}
					}
				}
			}
			else{
				for(String s:pksInTable) {
					noPks++;
				}
			}
		}
		// if we get to here then the primary key position was not found 
		return posInArrayList;
	}

	/**
	 * Method to return the position of the foreign key at tableName.colName in the 
	 * ArrayList fkData.
	 * @param tableName the String holding the table name
	 * @param colName the String holding the column name
	 * @return the position in the fkData arraylist, else null if not found
	 */
	public Integer getPosInFKArrayList(String tableName, String colName) {
		int noFKs = 0;
		Integer posInArrayList = null;

		// loop through the tables
		for(Table t: tables) {
			for(int i = 0; i < t.getNoCols(); i++) {
				// get the foreign key, if there is no FK then this will be null
				ForeignKey fk = t.getColumns().get(i).getForeignKey();
				if(fk != null) {
					if(t.getName().trim().equals(tableName)) {
						// the table that we want 
						if(t.getColumns().get(i).getName().trim().equals(colName)) {
							// this is the specific foreign key that we want 
							noFKs++;
							posInArrayList = noFKs-1;
							return posInArrayList;
						}
						else {
							// not the foreign key that we want, increment and move on
							noFKs++;
						}
					}
					else{
						// the foreign key is not null
						noFKs++;
					}
				}
			}
		}
		return posInArrayList;
	}

	/**
	 * Method to return the position of tableName, colName in the array which holds no primary 
	 * keys or foreign keys.
	 * @param tableName String holding the table name which we are trying to find
	 * @param colName String holding the column name which we are trying to find
	 * @return an Integer holding the position in the ArrayList, else null if not found.
	 */
	public Integer getPosInNonKeyArrayList(String tableName, String colName) {
		int index = 0;
		Integer posInArrayList = null;

		// loop through the tables
		for(Table t : tables) {
			for(int i = 0; i < t.getNoCols(); i++) {
				boolean primaryKey = t.getColumns().get(i).isPrimaryKey();
				ForeignKey fk = t.getColumns().get(i).getForeignKey();

				if(!primaryKey && fk == null) {
					// then the column is not a primary and has no foreign key
					if(t.getName().trim().equals(tableName) && t.getColumns().get(i).getName().trim().equals(colName.trim())){
						// this is the non key column we want, return the index in the list
						index++;
						posInArrayList = index-1;
						return posInArrayList;
					}
					else{
						// this is a non key column, but not the one we want 
						index++;
					}
				}
			}
		}
		return posInArrayList;
	}


	/**
	 * This method generates the population script, creates a certain number of examples (noExamples) for each 
	 * table and saves this script in 'popScript.txt'
	 * @param noExamples
	 */
	public void generatePopulationScript(int noExamples, GeneratePopulationScript genPopScreen) {
		this.genPopScreen = genPopScreen;
		PrintWriter popWriter = null;
		otherColumnData = new ArrayList<ArrayList<String>>();
		/*
		 * Step 1 is to populate all the primary keys with data. Each arraylist 
		 * should have data matching the number of examples.
		 */
		pkData = populatePkData(noExamples);

		// create a deep copy of pkData which we can remove elements from to ensure each primary key is unique
		ArrayList<ArrayList<String>> removablePkData = this.createDeepCopy(pkData);

		/*
		 * Step 2 is to populate all the foreign keys with data, they should be the 
		 * same as their corresponding primary keys.
		 */
		fkData = populateFkData(pkData);

		// create a deep copy of fkData incase the foreign key is also a primary key
		ArrayList<ArrayList<String>> removableFkData = this.createDeepCopy(fkData);


		/*
		 * Step 3 is where we are populating the INSERT INTO statements. If the column
		 * is a primary key then we get the position in the array list pkData and choose 
		 * at random an index of the arraylist. If this has already been chosen then it will 
		 * not be allowed. 
		 */
		try{
			popWriter = new PrintWriter("popScript.txt");
			for(int i = 0; i < tables.size(); i++){
				for(int examples = 0; examples < noExamples; examples++){
					String insertInto = "INSERT INTO ";
					insertInto += tables.get(i).getName() +"(";
					// loop to get the names of the columns in the tables and add their data sets to the arraylist dataSet
					for(int k = 0; k < tables.get(i).getNoCols(); k++) {
						insertInto += tables.get(i).getColumns().get(k).getName()+ ", ";
					}
					// remove the last ', ' as it is not required
					insertInto = insertInto.substring(0, insertInto.length()-2);
					insertInto += ") VALUES (";

					Random random = new Random();

					// next we need to deal with the three cases 
					for(int l = 0; l < tables.get(i).getNoCols(); l++) {

						if(tables.get(i).getPrimaryKey().contains(tables.get(i).getColumns().get(l).getName().trim())
								&& tables.get(i).getColumns().get(l).getForeignKey() == null) {

							// the column is a primary key, therefore get the arraylist that we can choose data from 
							int pos = getPosInPKArrayList(tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim());
							ArrayList<String> data = removablePkData.get(pos);
							// get random
							int rand = 0;
							if(data.size() > 0){
								rand = random.nextInt(data.size());
							}
							// insert the value at position rand
							String type = tables.get(i).getColumns().get(l).getType().toString();
							if(type.equals("VarChar") || type.equals("Character") || type.equals("Date")) {
								insertInto += "\'"+data.get(rand).trim()+"\', ";
							}
							else{
								insertInto += data.get(rand).trim()+", ";
							}
							removablePkData.get(getPosInPKArrayList(tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim())).remove(rand);
						}
						else if(tables.get(i).getColumns().get(l).getForeignKey() != null) {
							// the column is a foreign key, therefore get the foreign key and find the array list that corresponds to this
							ForeignKey fk = tables.get(i).getColumns().get(l).getForeignKey();

							// get pos in the fk arraylist
							int pos = getPosInFKArrayList(tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim());

							ArrayList<String> data = new ArrayList<String>();
							if(tables.get(i).getColumns().get(l).isPrimaryKey()) {
								data = removableFkData.get(pos);
							}
							else{
								data = fkData.get(pos);
							}
							// get a random position
							int rand = 0;
							if(data.size() > 0){
								rand = random.nextInt(data.size());
							}

							// add the value from data at pos rand to the statement
							// insert the value at position rand
							String type = tables.get(i).getColumns().get(l).getType().toString();
							if(type.equals("VarChar") || type.equals("Character") || type.equals("Date")) {
								insertInto += "\'"+data.get(rand).trim()+"\', ";
							}
							else{
								insertInto += data.get(rand).trim()+", ";
							}

							/* If the column is also a primary key then we cannot have duplicates in the data.
							 * Therefore, need to ensure this does not happen.
							 */
							if(tables.get(i).getColumns().get(l).isPrimaryKey()){
								// then the column is a primary key, therefore delete the used index from fkData.get(pos)
								removableFkData.get(pos).remove(rand);
							}
						}
						else {
							// the column is not a primary key or a foreign key, therefore get the data set
							String dataSet = tables.get(i).getColumns().get(l).getDataSet().trim();
							boolean unique = tables.get(i).getColumns().get(l).isUnique();
							ArrayList<String> data = new ArrayList<String>();
							// first time around get the data 
							if(examples == 0) {
								data = getRandomData(dataSet, noExamples, false, tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim());
								// add the data to otherColumnData
								otherColumnData.add(data);
							}
							else {
								data = otherColumnData.get(getPosInNonKeyArrayList(tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim()));
							}
							// get a random position
							int rand = 0;
							if(data.size() > 0){
								rand = random.nextInt(data.size());
							}
							// add the value from data at pos rand to the statement
							// insert the value at position rand
							String type = tables.get(i).getColumns().get(l).getType().toString();
							if(type.equals("VarChar") || type.equals("Character") || type.equals("Date")) {
								insertInto += "\'"+data.get(rand).trim()+"\', ";
							}
							else{
								insertInto += data.get(rand).trim()+", ";
							}
							if(unique) {	
								// since it is unique, then we need to remove the value at rand
								otherColumnData.get(getPosInNonKeyArrayList(tables.get(i).getName().trim(), tables.get(i).getColumns().get(l).getName().trim())).remove(rand);
							}
						}
					}

					// remove the last ', ' as it is not required
					insertInto = insertInto.substring(0, insertInto.length()-2);

					insertInto += ");";
					popWriter.println(insertInto);
				}
			}
			popWriter.flush();
			JOptionPane.showMessageDialog(null, "Population Script generated, saved in the same directory under 'popScript.txt'", "Information Message", JOptionPane.INFORMATION_MESSAGE);

		}catch(FileNotFoundException e) {
			System.out.println("popScript.txt file not found!");
		}
		finally{
			popWriter.close();
		}
		populatedPopulationScript = true;
	}

	/**
	 * Method to populate the primary key data. This must all be unique.
	 * @param noExamples int holding the number of examples to be created and hence the number of values that need 
	 * to be in the arraylist to ensure uniqueness
	 * @return the Arraylist<Arraylist<String>> of primary key data 
	 */
	public ArrayList<ArrayList<String>> populatePkData(int noExamples) {
		ArrayList<ArrayList<String>> pkData = new ArrayList<ArrayList<String>>();

		// loop through the tables getting the primary keys
		for(Table t : tables) {
			ArrayList<String> pksOfT = t.getPrimaryKey();
			for(String pk : pksOfT) {
				String fileName = t.getColumn(pk.trim()).getDataSet().trim();
				// add a new arraylist to pkData for the pk 
				ForeignKey fk = t.getColumn(pk.trim()).getForeignKey();
				if(fk == null) { 
					ArrayList<String> randomData = getRandomData(fileName, noExamples, true, t.getName().trim(), pk.trim());
					pkData.add(randomData);
				}
			}
		}
		return pkData;
	}

	/**
	 * Method to populate the foreign key data. This has to correspond to the relative primary key.
	 * @param pkData the primary key data 
	 * @return an ArrayList<ArrayList<String>> holding the foreign key data
	 */
	public ArrayList<ArrayList<String>> populateFkData(ArrayList<ArrayList<String>> pkData) {
		ArrayList<ArrayList<String>> fkData = new ArrayList<ArrayList<String>>();

		// loop through the tables getting the foreign keys
		for(Table t : tables) {
			for(int i = 0; i < t.getNoCols(); i++) {
				ForeignKey fk = t.getColumns().get(i).getForeignKey();
				if(fk != null) {
					int pos = getPosInPKArrayList(fk.getTable().getName().trim(), fk.getColumn().getName().trim());

					ArrayList<String> data = new ArrayList<String>();

					for(String s : pkData.get(pos)){
						data.add(s);
					}
					// add the arraylist to fkData 
					fkData.add(data);
				}
			}		
		}
		return fkData;
	}

	/**
	 * Method to populate an ArrayList<String> with random data from the file fileName. This will
	 * be done with use of the constraint programming toolkit Choco3
	 * @param fileName the file we want to get the data from
	 * @param noData the amount of data to populate
	 * @param pk boolean to determine if the column is a primary key and hence requires unique data
	 * @return the arraylist holding the data 
	 */
	public ArrayList<String> getRandomData(String fileName, int noData, boolean pk, String tableName, String colName) {
		ArrayList<String> data = new ArrayList<String>();
		try{
			FileReader reader = new FileReader(this.getFileName(fileName));
			Scanner sc = new Scanner(reader);

			ArrayList<String> fileInput = new ArrayList<String>();
			int lines = 0;

			while(sc.hasNextLine()) {
				// get the number of lines in the file
				fileInput.add(sc.nextLine());
				lines++;
			}

			// check list (this will be null if check has not been selected)
			ArrayList<Check> checks = getTable(tableName).getColumn(colName).getCheckList();
			boolean unique = getTable(tableName).getColumn(colName).isUnique();
			boolean notNull = getTable(tableName).getColumn(colName).isNotNull();
			String type = getTable(tableName).getColumn(colName).getType().toString().trim();

			boolean complete = false;
			while(!complete) {
				// use the number of lines to get a random line from the file
				Random random = new Random();
				int rand = random.nextInt(lines);
				// get the type
				IntVar [] constrainedData;
				if(checks == null && !unique) {
					/* we dont need to go to the constraint solver as the data does not have to be unique and 
					 * it has no check constraints.
					 */
					if(notNull) {
						// the value cannot be null
						if(fileInput.get(rand) != null) {
							data.add(fileInput.get(rand));
						}
					}
					else{
						// it can be null or not null
						data.add(fileInput.get(rand));
					}
					if(data.size() == noData) {
						complete = true;
					}
				}
				else {
					// enter the else if at least one check exists or uniqueness constraint
					if(type.equals("VarChar")) {
						// go to VarCharSolver to find a solution to satisfy the constraints
						VarCharSolver varcharSolver = new VarCharSolver(getFileName(fileName), checks, unique, notNull, type, noData);
						if(varcharSolver.solve()) {
							// a solution has been found
							VarCharValidator validator = new VarCharValidator(varcharSolver.getStringData(), checks, notNull, unique, noData);
							if(validator.validate()) {
								for(String s: varcharSolver.getStringData()) {
									data.add(s);
								}
								// we can exit the loop therefore set complete as true
								complete = true;
							}
							else{
								// validation has not passed
								throw new ContradictionException();
							}
						}
						else{
							// there was no solution found
							throw new ContradictionException();
						}
					}
					else if(type.equals("Integer")) {
						// go to IntegerSolver to find a solution to satisfy the constraints
						IntegerSolver intSolver = new IntegerSolver(getFileName(fileName),  checks, unique, notNull, type, noData);
						if(intSolver.solve()) {
							// get the solution
							constrainedData = intSolver.getData();
							IntegerValidator intValidator = new IntegerValidator(constrainedData, checks, notNull, unique, noData);
							if(intValidator.validate()) {
								// loop through the IntVar array and put the values into the data arraylist
								for(int i = 0; i < noData; i++) {
									data.add(i, String.valueOf(constrainedData[i].getValue()));
								}
								// we can exit the loop therefore set complete as true
								complete = true;
							}
							else {
								// validation has not passed
								throw new ContradictionException();
							}
						}
						else{
							// there was no solution found
							throw new ContradictionException();
						}
					}
					else if(type.equals("Date")){
						// go to DateSolver to find a solution to satisfy the constraints
						DateSolver dateSolver = new DateSolver(getFileName(fileName), checks, unique, notNull, type, noData);
						if(dateSolver.solve()) {
							// a solution has been found 
							constrainedData = dateSolver.getData();
							DateValidator dateValidator = new DateValidator(dateSolver.getDateData(), checks, notNull, unique, noData);
							if(dateValidator.validate()) {
								// get the actual dates
								for(String d: dateSolver.getDateData()) {
									data.add(d);
								}
								// we can exit the loop therefore set complete as true
								complete = true;
							}
							else{
								// validation has not passed
								throw new ContradictionException();
							}
						}
						else{
							// there was no solution found
							throw new ContradictionException();
						}
					}
					else if(type.equals("Character")){
						// go to CharacterSolver to find a solution to satisfy the constraints
						CharacterSolver charSolver = new CharacterSolver(getFileName(fileName), checks, unique, notNull, noData);
						if(charSolver.solve()) {
							// a solution has been found
							constrainedData = charSolver.getData();
							CharacterValidator charValidator = new CharacterValidator(charSolver.getCharacterData(), checks, notNull, unique, noData);
							if(charValidator.validate()) {
								for(char c : charSolver.getCharacterData()) {
									data.add(""+c);
								}
								// we can exit the loop therefore set complete as true
								complete = true;							
							}
							else{
								// validation has not passed
								throw new ContradictionException();
							}
						}
						else{
							// there was no solution found
							throw new ContradictionException();
						}
					}
				}
			}
		}
		catch(FileNotFoundException | ContradictionException e) {
			System.out.println("File not found/Contradiction");
		}
		return data;
	}


	/**
	 * Method to return a deep copy of the ArrayList<ArrayList<String>> entered.
	 * @param data ArrayList<ArrayList<String>> to be copied
	 * @return the copied ArrayList<ArrayList<String>> removableData
	 */
	public ArrayList<ArrayList<String>> createDeepCopy(ArrayList<ArrayList<String>> data) {
		ArrayList<ArrayList<String>> removableData = new ArrayList<ArrayList<String>>();
		// deep copy of pkData
		for(ArrayList<String> list : data) {
			ArrayList<String> temp = new ArrayList<String>();
			for(String s : list) {
				temp.add(s);
			}
			removableData.add(temp);
		}
		return removableData;
	}

	/**
	 * This method downloads the SQL examples produced, these are saved in a file named 
	 * 'SQL_Examples.txt'
	 * @param examples - Arraylist holding the SQL examples 
	 */
	public void downloadExamplesFile(ArrayList<String> examples) {
		PrintWriter writer = null;
		try{
			writer = new PrintWriter("SQL_Examples.txt");
			for(int i = 0; i < examples.size(); i++) {
				writer.println(examples.get(i).toString());
			}
			writer.flush();
			JOptionPane.showMessageDialog(null, "SQL examples file downloaded, saved in 'SQL_Examples.txt'", "Information Message", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(FileNotFoundException e) {
			System.out.println("File not found");
		}
		finally {
			writer.close();
		}
	}

	/**
	 * This method checks the input against different options and returned is the corresponding 
	 * file name
	 * @param input
	 * @return file - String holding the file name
	 */
	public String getFileName(String input) {
		String file = "";
		if(input.equals("Names (concatenated first and surnames)")){
			file = "concatenatedNames.txt";
		}
		else if(input.equals("Course Names")) {
			file = "courseNames.txt";
		}
		else if(input.equals("First Names")) {
			file = "firstNames.txt";
		}
		else if(input.equals("Surnames")) {
			file = "surnames.txt";
		}
		else if(input.equals("Integers")) {
			file = "randInts.txt";
		}
		else if(input.equals("Addresses")) {
			file = "addresses.txt";
		}
		else if(input.equals("Contact Numbers")) {
			file = "contactNumbers.txt";
		}
		else if(input.equals("ID (8 length integer)")){
			file = "ID_length8.txt";
		}
		else if(input.equals("Dates of Birth")) {
			file = "dobs.txt";
		}
		else if(input.equals("Random String")) {
			file = "randomStrings.txt";
		}
		else if(input.equals("Random Characters")) {
			file = "randChars.txt";
		}
		return file;
	}

	/**
	 * This method builds the SQL examples by using the user input contained in chosen
	 * @param chosen - ChosenSqlKeywords holds the user input for the type of sql required
	 * @return sqlBuilder - ArrayList<String> holding all statements generated
	 */
	public ArrayList<String> getSqlExamples() {

		// create the string builder 
		ArrayList<String> sqlBuilder = new ArrayList<String>();

		// if first of all we make an example for each ticked box then we can combine later 

		// get a random col and table
		Random r = new Random();
		int randTable = r.nextInt(tables.size());
		int randCol = r.nextInt(getTables().get(randTable).getNoCols());

		/* 
		 * to ensure there is always at least one query returned, add a generic select
		 * statement.
		 */
		sqlBuilder.add("SELECT * FROM "+getTables().get(randTable).getName()+";");

		// distinct
		if(chosen.isDistinct()) {
			String query = "SELECT DISTINCT "+getTables().get(randTable).getColumns().get(randCol).getName()+" FROM "+getTables().get(randTable).getName()+";";
			sqlBuilder.add(query);
		}

		// get the type of join we'll be using
		String joinType = chosen.getJoinType();

		// where (and, between, or, in)
		while(chosen.isAnd() || chosen.isBetween() || chosen.isOr() || chosen.isIn() || chosen.isEquals()){
			if(chosen.isWhere()) {
				// where has been chosen 
				String query = "";
				int rand = r.nextInt(tables.size());
				String tableName = getTables().get(rand).getName().trim();


				String col1Name = getTable(tableName).getColumns().get(r.nextInt(getTables().get(rand).getNoCols())).getName();
				String col2Name = getTable(tableName).getColumns().get(r.nextInt(getTables().get(rand).getNoCols())).getName();


				String originalQuery = "SELECT "+ getTable(tableName).getColumns().get(r.nextInt(getTable(tableName).getNoCols())).getName()+" FROM "
						+ tableName +" WHERE "+tableName+"."+col1Name;

				query += originalQuery;

				// get the position in the relevant data for col1Name
				ArrayList<String> col1Data = new ArrayList<String>();
				ArrayList<String> col2Data = new ArrayList<String>();

				if(populatedPopulationScript) {
					col1Data = getData(tableName, col1Name);
					col2Data = getData(tableName, col2Name);
				}

				// get the types of col1 and col2
				String type1 = getTables().get(rand).getColumn(col1Name).getType().toString().trim();
				String type2 = getTables().get(rand).getColumn(col2Name).getType().toString().trim();

				/* and and between come together therefore check if they have both been ticked
				 * before producing a statement using between.
				 */
				if(chosen.isAnd() && chosen.isBetween()) {
					if(!col1Data.isEmpty()){
						// the user has used the system to generate the population script 
						if(type1.equals("Date")) {
							query += " BETWEEN '"+col1Data.get(r.nextInt(col1Data.size()))+"' AND '"+col1Data.get(r.nextInt(col1Data.size()))+"'";
						}
						else if(type1.equals("Integer")){
							query += " BETWEEN "+col1Data.get(r.nextInt(col1Data.size()))+" AND "+col1Data.get(r.nextInt(col1Data.size()));
						}
						else {
							System.out.println("Your column is of type varchar or character which cannot have a sensible between statement");
						}
					}
					else{
						if(type1.equals("Date")){
							query += " BETWEEN 'VALUE_1' AND 'VALUE_2'";
						}
						else if(type1.equals("Integer")) {
							query += " BETWEEN VALUE_1 AND VALUE_2";
						}
						else{
							System.out.println("Your column is of type varchar or character which cannot have a sensible between statement");
						}
					}
					// set and and between false
					chosen.setAnd(false);
					chosen.setBetween(false);
				}
				// and can appear on its own, therefore we will only check if and has been checked.
				else if(chosen.isAnd()) {
					// if the data sets for both col1Name and col2Name are not empty then the user has populated the population script.
					if(!col1Data.isEmpty() && !col2Data.isEmpty()) {
						// the user has used the system to generate the population script
						if(type1.equals("VarChar") || type1.equals("Date") || type1.equals("Character")) {
							query += " = '"+col1Data.get(r.nextInt(col1Data.size()))+"' AND ";
						}
						else {
							query += " = "+col1Data.get(r.nextInt(col1Data.size()))+" AND ";
						}

						if(type2.equals("VarChar") || type2.equals("Date") || type2.equals("Character")) {
							query += tableName+"."+col2Name+" = '"+col2Data.get(r.nextInt(col2Data.size()))+"'";
						}
						else {
							query += tableName+"."+col2Name+" = "+col2Data.get(r.nextInt(col2Data.size()));
						}
					}
					else{
						if(type2.equals("Integer")){
							query += " = VALUE_1 AND "+tableName+"."+col2Name+" = VALUE_2";							
						}
						else{
							query += " = 'VALUE_1' AND "+tableName+"."+col2Name+" = 'VALUE_2'";
						}
					}
					chosen.setAnd(false);
				}
				// or can also appear on its own
				else if(chosen.isOr()) {
					if(!col1Data.isEmpty() && !col2Data.isEmpty()) {
						// the user has used the system to generate the population script
						if(type1.equals("VarChar") || type1.equals("Character") || type1.equals("Date")){
							query += " = '"+col1Data.get(r.nextInt(col1Data.size()))+"' OR ";
						}
						else{
							query += " = "+col1Data.get(r.nextInt(col1Data.size()))+" OR ";
						}

						if(type2.equals("VarChar") || type2.equals("Character") || type2.equals("Date")) {
							query += tableName+"."+col2Name+" = '"+col2Data.get(r.nextInt(col2Data.size()))+"'";
						}
						else {
							query += tableName+"."+col2Name+" = "+col2Data.get(r.nextInt(col2Data.size()));
						}
					}
					else{
						query += " = 'VALUE_1' OR "+tableName+"."+col2Name+" = 'VALUE_2'";
					}
					chosen.setOr(false);
				}
				// in can appear on its own 
				else if(chosen.isIn()) {
					if(!col1Data.isEmpty()){
						// the user has used the system to generate the population script
						if(type1.equals("Integer")){
							query += " IN ("+col1Data.get(r.nextInt(col1Data.size()))+", "+col1Data.get(r.nextInt(col1Data.size()))+", "+col1Data.get(r.nextInt(col1Data.size()))+")";
						}
						else{
							query += " IN ('"+col1Data.get(r.nextInt(col1Data.size()))+"', '"+col1Data.get(r.nextInt(col1Data.size()))+"', '"+col1Data.get(r.nextInt(col1Data.size()))+"')";
						}
					}
					else{
						if(type1.equals("Integer")) {
							query += " IN (VALUE_1, VALUE_2, ..., VALUE_N)";
						}
						else{
							query += " IN ('VALUE_1', 'VALUE_2', ..., 'VALUE_N')";
						}
					}
					chosen.setIn(false);
				}
				else if(chosen.isEquals()) {
					if(!col1Data.isEmpty()){
						// the user has used the system to generate the population script
						if(type1.equals("Integer")){
							query += " = "+col1Data.get(r.nextInt(col1Data.size()));
						}
						else{
							query += " = '"+col1Data.get(r.nextInt(col1Data.size()))+"'";
						}
					}
					else{
						if(type1.equals("Integer")) {
							query += " = VALUE_1";
						}
						else{
							query += " = 'VALUE_1'";
						}
					}
					chosen.setEquals(false);
				}
				else{
					// there is no operator to correspond with where, therefore exit the loop
					break;
				}

				if(!query.equals(originalQuery)){
					// we will check if group by and order by have been checked and add these onto the where statement.
					if(chosen.isGroupBy()) {
						query += " GROUP BY "+tableName+"."+col1Name;
						chosen.setGroupBy(false);
					}

					if(chosen.isOrderBy()) {
						query += " ORDER BY "+tableName+"."+col1Name;
						if(chosen.isAsc()){
							query += " ASC";
						}
						else if(chosen.isDesc()) {
							query += " DESC";
						}
						chosen.setOrderBy(false);
					}
					query += ";";
					// add this query to the array list of queries
					sqlBuilder.add(query);
				}
			}
		}

		/* Functions appear in queries separately from other keywords, therefor
		 * these will be dealt with individually
		 */
		// count 
		if(chosen.isCount()) {
			sqlBuilder.add(aggregationBuilder("COUNT", r));
		}

		// sum 
		if(chosen.isSum()) {
			sqlBuilder.add(aggregationBuilder("SUM", r));
		}

		// avg
		if(chosen.isAvg()) {
			sqlBuilder.add(aggregationBuilder("AVG", r));
		}

		// first
		if(chosen.isFirst()) {
			sqlBuilder.add(aggregationBuilder("FIRST", r));
		}

		// last 
		if(chosen.isLast()) {
			sqlBuilder.add(aggregationBuilder("LAST", r));
		}

		// max
		if(chosen.isMax()) {
			sqlBuilder.add(aggregationBuilder("MAX", r));
		}

		// min 
		if(chosen.isMin()) {
			sqlBuilder.add(aggregationBuilder("MIN", r));
		}

		// ucase
		if(chosen.isUcase()) {
			sqlBuilder.add(aggregationBuilder("Ucase", r));
		}

		// lcase
		if(chosen.isLcase()) {
			sqlBuilder.add(aggregationBuilder("Lcase", r));
		}

		// Next look at the join possibilities

		String query = "";
		// create an array list to hold the foreign keys of each column in question
		ArrayList<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();
		/* 
		 * if there is a foreign key in the schema then we can have a join, 
		 * otherwise we cannot 
		 */
		boolean containsFK = false;
		boolean fkAdded = false;
		// create an array list of tables in which each of these tables will have one or more columns with foreign keys
		ArrayList<Table> validTables = new ArrayList<Table>();
		// loop through all tables and if the table contains a column with a foreign key, add it to validTables
		for(Table t : tables) {
			for(int i = 0; i < t.getNoCols(); i++) {
				if(t.getColumns().get(i).getForeignKey() != null) {// && t.getColumns().get(i).getForeignKey().getTable() != null
					//		&& t.getColumns().get(i).getForeignKey().getColumn() != null){
					containsFK = true;
					validTables.add(t);
				}
			}
		}

		// keep looping whilst fk is empty - we need foreign keys for the join to happen!
		while(!fkAdded && containsFK && !chosen.getJoinType().equals("none")) {
			// reset to an empty string
			query = "";
			// get a random table to start with
			String t1Name = validTables.get(r.nextInt(validTables.size())).getName();
			String t2Name = "";
			String foreignKey = "";
			String primaryKey = "";

			// get the foreign keys in the table and add them (at the index equal to the column number 
			for(int i = 0; i < getTable(t1Name).getNoCols(); i++){
				ForeignKey f = getTable(t1Name).getColumns().get(i).getForeignKey();
				if(f != null) {// && f.getTable() != null && f.getColumn() != null){
					// add the foreign key at the index
					foreignKeys.add(i, f);
					fkAdded = true;
				}
				else{
					// add null at the index
					foreignKeys.add(i, null);
				}
			}

			if(!foreignKeys.isEmpty()){
				ForeignKey f = null;
				int index = -1; // invalid index
				while(f == null){
					// get a random index so that the query is not always for the same foreign key
					index = r.nextInt(foreignKeys.size());
					// get the foreign key related to that index
					f = foreignKeys.get(index);
				}
				// get the table which is in the foreign key
				t2Name = f.getTable().getName();
				// get the column in table 1 this foreign key happens at 
				foreignKey = getTable(t1Name).getColumns().get(index).getName().trim();
				// get the primary key - i.e the column related to the foreign key
				primaryKey = f.getColumn().getName().trim();
			}

			/* if fk is no longer empty then we can build the query as we need both primary key and
			 * foreign key to be populated for the query to work.
			 */ 
			if(!foreignKey.isEmpty() && !primaryKey.isEmpty() && !t2Name.isEmpty() && !t1Name.isEmpty()) {
				// get random columns from table 1 and table 2
				String t1ColName = getTable(t1Name).getColumns().get(r.nextInt(getTable(t1Name).getNoCols())).getName();
				String t2ColName = getTable(t2Name).getColumns().get(r.nextInt(getTable(t2Name).getNoCols())).getName();

				query += "SELECT "+t1Name+"."+t1ColName +", "+t2Name+"."+t2ColName+" FROM ";

				if(joinType.equals("inner")){
					query+= t2Name+" INNER JOIN "+t1Name+" ON "+t2Name+"."+primaryKey+" = "+t1Name+"."+foreignKey;
				}
				else if(joinType.equals("left")) {
					query += t2Name+ " LEFT JOIN "+t1Name+" ON "+t2Name+"."+primaryKey+" = "+t1Name+"."+foreignKey;
				}
				else if(joinType.equals("right")) {
					query += t1Name+" RIGHT JOIN "+t2Name+" ON "+t1Name+"."+foreignKey+" = "+t2Name+"."+primaryKey;
				}
				else if(joinType.equals("fullOuter")) {
					query += t2Name+" FULL OUTER JOIN "+t1Name+" ON "+t2Name+"."+primaryKey+" = "+t1Name+"."+foreignKey;
				}

				if(chosen.isGroupBy()) {
					query += " GROUP BY "+t1Name+"."+t1ColName+";";
				}
				else if(chosen.isOrderBy()) {
					query += " ORDER BY "+t1Name+"."+t1ColName;
					if(chosen.isAsc()) {
						query += " ASC;";
					}
					else if(chosen.isDesc()) {
						query += " DESC;";
					}
				}
				else{ 
					query += ";";
				}
			}
		}
		// if it does not end with 'FROM' then it can be added to sqlBuilder as it is a full query
		if(!query.endsWith("FROM ") && !query.isEmpty()){
			sqlBuilder.add(query);
		}
		return sqlBuilder;
	}

	/**
	 * Method to return the data at a particular position of the relevant arraylist
	 * @param tableName 
	 * @param colName
	 * @return
	 */
	public ArrayList<String> getData(String tableName, String colName) {
		ArrayList<String> colData = new ArrayList<String>();

		if(getTable(tableName).getPrimaryKey().contains(colName) &&
				getTable(tableName).getColumn(colName).getForeignKey() == null) {
			// col1Name is a primary key but has no foreign key
			colData = pkData.get(getPosInPKArrayList(tableName, colName));
		}
		else if(getTable(tableName).getColumn(colName).getForeignKey() != null) {
			// col1Name has a foreign key
			colData = fkData.get(getPosInFKArrayList(tableName, colName));
		}
		else {
			// col1Name is not a primary key and has no foreign key
			colData = otherColumnData.get(getPosInNonKeyArrayList(tableName, colName));
		}

		return colData;
	}

	/**
	 * Mutator for chosenSqlKeywords
	 * @param distinct
	 * @param where
	 * @param and
	 * @param or
	 * @param between
	 * @param in
	 * @param equals
	 * @param groupBy
	 * @param orderBy
	 * @param asc
	 * @param desc
	 * @param selectedJoin
	 * @param count
	 * @param sum
	 * @param avg
	 * @param first
	 * @param last
	 * @param max
	 * @param min
	 * @param ucase
	 * @param lcase
	 */
	public void setChosenSqlKeywords(boolean distinct, boolean where, boolean and, 
			boolean or, boolean between, boolean in, boolean equals,  boolean groupBy, 
			boolean orderBy, boolean asc, boolean desc, String selectedJoin,
			boolean count, boolean sum, boolean avg, boolean first, boolean last, boolean max, boolean min, 
			boolean ucase, boolean lcase){
		chosen = new ChosenSqlKeywords(distinct, where, and, or, between, in, equals, groupBy, 
				orderBy, asc, desc, selectedJoin,count, sum, avg, first, last, max, min, ucase, lcase);
	}

	/**
	 * This method builds the beginning of an aggregation statements and returns this as a string
	 * @param aggregation - String holding the type of function
	 * @param r - Random to produce a random int
	 * @return string holding the beginning of the statement
	 */
	public String aggregationBuilder(String aggregation, Random r) {
		int rand = r.nextInt(tables.size());
		return "SELECT "+aggregation+"("+getTables().get(rand).getColumns().get(r.nextInt(getTables().get(rand).getNoCols())).getName()+") FROM "+getTables().get(rand).getName()+";";
	}
}
