import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InitialScreen extends GUI implements ActionListener {
	private JLabel nameLabel, noTablesLabel;
	private JTextField nameField;
	private JComboBox noTablesCombo;
	private JButton next;
	
	private AddTableInfo tInfo = null;
	
	/**
	 * Constructor for InitialScreen
	 * @param db - Database instance
	 */
	public InitialScreen(Database db) {
		this.db = db;
		this.initialSetUp();
	}
	
	@Override
	public void initialSetUp() {
		panel = new JPanel();
		JPanel grid = new JPanel(new GridLayout(2,2));
		nameLabel = new JLabel("Name of database:");
		noTablesLabel = new JLabel("Number of tables:");
		nameField = new JTextField(30);
		noTablesCombo = new JComboBox();
		next = new JButton("Enter table information");
		
		// populate the combo box
		this.populateComboBox(noTablesCombo);
		
		// add the components to the panels 
		grid.add(nameLabel);
		grid.add(nameField);
		grid.add(noTablesLabel);
		grid.add(noTablesCombo);
		panel.add(next);
		
		// add the panels to the JFrame
		this.add(grid, BorderLayout.CENTER);
		this.add(panel, BorderLayout.SOUTH);
		
		// add the actionlisteners
		next.addActionListener(this);
	}

	/**
	 * This method populates the noTables combo box with options
	 * @param noTablesCombo - the JComboBox to be populated
	 */
	public void populateComboBox(JComboBox noTablesCombo){
		int max_tables = 20;
		for(int i = 1; i <= max_tables; i++){
			noTablesCombo.addItem(i);
		}
		/* set the selected index to be before the actionlistener - this
		 *  means that the user will have no selected number of tables to 
		 *  start with.
		 */
		noTablesCombo.setSelectedIndex(-1);
	}
	
	/**
	 * This method checks if the db name entered and the number of tables. If the database does not already 
	 * exist, a new instance of Database will be created, else the existing instance will be updated accordingly
	 * following the assumption described below.
	 */
	public void makeNewDatabase() {
		// make a new database object
		if(!nameField.getText().trim().equals("") && noTablesCombo.getSelectedItem() != null){
			String dbName = nameField.getText().trim();
			int noTables = (int)noTablesCombo.getSelectedItem();
			
			/* Make an assumption: If the user has pressed back on the AddTableInfo screen,
			 * and set the no. of tables to be a lower number, create a new instance of 
			 * Database and send it to a new instance of AddTableInfo.
			 */
			if(db.getDbName() != null) {
				if(db.getDbName().equals(dbName)) {
					if(db.getNoTables() > noTables) {
						// reset to a new database
						db = new Database();
						db.setDbName(dbName);
						db.setNoTables(noTables);
						// send this new db to tInfo 
						tInfo = new AddTableInfo(db, this);
					}
					else if(db.getNoTables() == noTables) {
						// everything should just go back to how it was 
						tInfo.setVisible(true);
					}
					else{
						// if the noTables is more than the previously set, update tInfo
						db.setNoTables(noTables);
						tInfo.refresh();
					}
				}
				else{
					db = new Database();
					db.setDbName(dbName);
					db.setNoTables(noTables);
					// send this new db to tInfo
					tInfo = new AddTableInfo(db, this);
				}
			}
			else{
				db.setDbName(dbName);
				db.setNoTables(noTables);
				tInfo = new AddTableInfo(db, this); 
			}
			tInfo.setVisible(true);
			// close the current screen
			this.dispose();
		}
		else{
			if(nameField.getText().trim().isEmpty()){
				JOptionPane.showMessageDialog(null, "Please enter the name of the database", "Input Error", JOptionPane.ERROR_MESSAGE);
			}
			if(noTablesCombo.getSelectedItem() == null){
				JOptionPane.showMessageDialog(null, "Please choose the number of tables", "Input Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	/**
	 * This method sets the tInfo parameter and also makes the InitialScreen again visible
	 * @param tInfo - instance of AddTableInfo
	 */
	public void refresh(AddTableInfo tInfo) {
		this.tInfo = tInfo;
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == next){
			makeNewDatabase();
		}
	}
}
