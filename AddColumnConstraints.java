import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddColumnConstraints extends GUI implements ActionListener, ItemListener, FocusListener {
	private static final int MAX_CHECKS = 4; // implementation decision
	private String tableName;
	private String colName;
	private Object colType;
	private Integer size;
	private AddColumnInfo colInfo;
	private int indexOfRow;
	private boolean pk; // determines whether the primary key option has been selected.
	// JComponents 
	private JLabel nameLabel, typeLabel, constraintsLabel,defaultLabel;
	private JTextField name, type, defaultField;
	private JCheckBox notNull, check, unique, defaultCheck;
	private JButton back, saveReturn, addNewCheck;
	private JPanel colFields, columnInfoPanel, constraintPanel, baseCheckPanel, defaultPanel, checkBoxPanel, checkInfoPanel;
	private ArrayList<JPanel> checkPanel;

	private ArrayList<JTextField> checkVal;
	private ArrayList<JComboBox> checkOpOptions;
	private ArrayList<JLabel> checkLabel;
	private ArrayList<JButton> deleteCheck;
	private int checkPos = 0; // the current position of the check arraylists
	
	private boolean notNullEntered, checkEntered, uniqueEntered, defaultEntered = false; //originally

	/**
	 * Constructor for AddColumnConstraints
	 * @param tableName
	 * @param colName
	 * @param colType
	 * @param size
	 * @param colInfo
	 * @param index
	 * @param db
	 * @param pk
	 */
	public AddColumnConstraints(String tableName, String colName, Object colType, Integer size, AddColumnInfo colInfo, int index, Database db, boolean pk){
		this.tableName = tableName;
		this.colName = colName;
		this.colType = colType;
		this.size = size;
		this.colInfo = colInfo;
		this.indexOfRow = index;
		this.db = db;
		this.pk = pk;

		this.setSize(600,130);
		this.setTitle("Schema Generation : Add Column Constraints");
		
		this.initialSetUp();
	}

	@Override
	public void initialSetUp() {

		// North area of the JFrame - column name, type and size
		nameLabel = new JLabel("Column Name: ");
		name = new JTextField(colName.length());
		name.setText(colName);
		name.setEnabled(false);

		typeLabel = new JLabel("Type: ");
		type = new JTextField(colType.toString().length());
		type.setText(colType.toString());
		type.setEnabled(false);

		columnInfoPanel = new JPanel(new GridLayout(2,1));
		colFields = new JPanel();

		colFields.add(nameLabel);
		colFields.add(name);
		colFields.add(typeLabel);
		colFields.add(type);

		// add this to the North
		columnInfoPanel.add(colFields);

		// Centre area of the JFrame - constraints grid 
		constraintsLabel = new JLabel("Constraints: ");

		// for later - check
		baseCheckPanel  = new JPanel(new GridLayout(6,1)); // this is the panel that all check info will be added to
		checkPanel = new ArrayList<JPanel>();
		checkInfoPanel = new JPanel();
		constraintPanel = new JPanel();

		checkLabel = new ArrayList<JLabel>();
		checkOpOptions = new ArrayList<JComboBox>();
		checkVal = new ArrayList<JTextField>();
		deleteCheck = new ArrayList<JButton>();
		addNewCheck = new JButton("Add Another Check");
		addNewCheck.addActionListener(this);

		// for later - default 
		defaultPanel = new JPanel();

		defaultLabel = new JLabel("Default Value: ");
		defaultField = new JTextField(30);

		/* this column will never be null at this point as it has already been created 
		 * therefore we can go straight to populateEnteredInformation() and enter any
		 * constraints which already exist.
		 */
		this.populateEnteredInformation();
		
		check.addItemListener(this);
		defaultCheck.addItemListener(this);

		checkBoxPanel = new JPanel();

		checkBoxPanel.add(constraintsLabel);
		checkBoxPanel.add(notNull);
		checkBoxPanel.add(check);
		checkBoxPanel.add(unique);
		checkBoxPanel.add(defaultCheck);

		// add the checkBoxPanel to the columnInfoPanel then add to the north of the main JFrame
		columnInfoPanel.add(checkBoxPanel);
		this.add(columnInfoPanel, BorderLayout.NORTH);

		// South area of the JFrame - button panel
		back = new JButton("Back");
		saveReturn = new JButton("Save and Return");

		// add action listeners 
		back.addActionListener(this);
		saveReturn.addActionListener(this);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(back);
		buttonPanel.add(saveReturn);

		this.add(buttonPanel, BorderLayout.SOUTH);

		defaultPanel.add(defaultLabel);
		defaultPanel.add(defaultField);
	}

	/** 
	 * This method populates the JComponents dependent on whether there are constraints already entered.
	 */
	public void populateEnteredInformation() {
		// the column has already been created therefore pull back the entered info
		notNull = new JCheckBox("Not Null", db.getTable(tableName).getColumn(colName).isNotNull());
		check = new JCheckBox("Check", db.getTable(tableName).getColumn(colName).isCheck());
		unique = new JCheckBox("Unique", db.getTable(tableName).getColumn(colName).isUnique());
		defaultCheck = new JCheckBox("Default", db.getTable(tableName).getColumn(colName).isDefaultCheck());

		// default 
		if(db.getTable(tableName).getColumn(colName).isDefaultCheck()){
			defaultField.setText(db.getTable(tableName).getColumn(colName).getDefaultVal());
			addDefaultToScreen();
		}

		// check when we have an arraylist
		if(db.getTable(tableName).getColumn(colName).isCheck()){
			for(int i = 0; i < db.getTable(tableName).getColumn(colName).getCheckList().size(); i++){
				// populate the check fields
				JComboBox tempOp = new JComboBox();
				this.populateCheckCombo(tempOp);
				checkOpOptions.add(i, tempOp);
				// set selected item
				checkOpOptions.get(i).setSelectedItem(db.getTable(tableName).getColumn(colName).getCheckList().get(i).getCheckOperator().trim());

				JTextField tempVal = new JTextField(30);
				checkVal.add(i, tempVal);
				// set the selected value
				checkVal.get(i).setText(db.getTable(tableName).getColumn(colName).getCheckList().get(i).getValue());
				addCheckSelected();
			}
		}
	}
	
	/**
	 * This method disposes the current screen and sends control to an instance of AddColumnInfo
	 */
	public void returnToPreviousPage() {
		colInfo.refresh();
		this.dispose();
	}

	/**
	 * This method records and validates all user input and adds the constraints to the specific 
	 * column.
	 */
	public void captureEnteredInformation() {
		// validation booleans
		ArrayList<Boolean> checkValidated = new ArrayList<Boolean>();
		boolean defaultValidated = true;

		// capture check box info 
		notNullEntered = notNull.isSelected();
		uniqueEntered = unique.isSelected();
		checkEntered = check.isSelected();
		defaultEntered = defaultCheck.isSelected();

		String defaultVal = "";
		ArrayList<Check> checks = new ArrayList<Check>();
		
		// Validation 
		if(defaultEntered){
			if(!defaultField.getText().isEmpty()){
				defaultVal = defaultField.getText().trim();
				defaultValidated = true;
			}
			else{
				JOptionPane.showMessageDialog(null, "Please enter the default value.", "Error", JOptionPane.ERROR_MESSAGE);
				defaultValidated = false;
			}
		}
		if(checkEntered){
			for(int i = 0; i < checkPos; i++){
				if(checkOpOptions.get(i).getSelectedItem() != null && !checkVal.get(i).getText().isEmpty() && !checkVal.get(i).getText().equals("Enter Check Value")){
					if(colType.toString().equals("Integer") || colType.toString().equals("VarChar")) {
						try{
							int val = Integer.parseInt(checkVal.get(i).getText().trim());
							// CHANGE CHECK TO USE INTEGERS IF WE ARE KEEPING THIS AS THE ONLY OPTION
							checks.add(new Check(checkOpOptions.get(i).getSelectedItem().toString(), checkVal.get(i).getText().trim()));
							checkValidated.add(true);
						}
						catch(NumberFormatException e) {
							JOptionPane.showMessageDialog(null, "At the moment, the program only supports values that are integers", "Error Message", JOptionPane.ERROR_MESSAGE);
							checkVal.get(i).setText("");
							checkValidated.add(false);
						}
					}
					else if(colType.toString().equals("Date")) {
						try{
							String input = checkVal.get(i).getText().trim();
							if(input.length() != 4) {
								throw new NumberFormatException();
							}
							// ensure that the input is an int
							Integer.parseInt(checkVal.get(i).getText().trim());
							checks.add(new Check(checkOpOptions.get(i).getSelectedItem().toString(), checkVal.get(i).getText().trim()));
							checkValidated.add(true);
						}
						catch(NumberFormatException e) {
							JOptionPane.showMessageDialog(null, "At the moment, the program only supports date checks based on year."
									+ "\nPlease enter the comparitive year in the format YYYY.", "Input Error Message", JOptionPane.ERROR_MESSAGE);
							checkVal.get(i).setText("");
							checkValidated.add(false);
						}
					}
					else if(colType.toString().equals("Character")) {
						try {
							String input = checkVal.get(i).getText().trim();
							// the length must be 1 as we are going to compare it to another letter in the alphabet
							if(input.length() != 1) {
								throw new IOException();
							}
							// ensure that the input is a character
							if(!input.matches("[a-zA-Z]+")) {
								throw new IOException();
							}
							else {
								// the length of string entered is 1 and is alphabetic
								checks.add(new Check(checkOpOptions.get(i).getSelectedItem().toString(), checkVal.get(i).getText().trim()));
								checkValidated.add(true);
							}
						}
						catch(IOException e) {
							JOptionPane.showMessageDialog(null, "Please enter a single character from the alphabet", "Input Error", JOptionPane.ERROR_MESSAGE);
							checkVal.get(i).setText("");
							checkValidated.add(false);
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Please enter the information of the check.", "Error", JOptionPane.ERROR_MESSAGE);
					checkValidated.add(false);
				}
			}
		}

		if(!checkValidated.contains(false) && defaultValidated){
			if(db.addConstraintsToColumn(tableName, colName, notNullEntered, uniqueEntered, checkEntered, checks, defaultEntered, defaultVal)){
				this.dispose();
				colInfo.setConstraintsButton(indexOfRow);
				colInfo.setVisible(true);
			}
		}
	}

	/**
	 * This method adds the check functionality use of helper method addCheck()
	 */
	public void addCheckSelected() {
		// set the size to include space for the check and default
		this.setSize(900,500);
		// if the check info string has not been added then add it
		if(!baseCheckPanel.isAncestorOf(checkInfoPanel)) {
			this.addCheckInfo();
		}
		this.addCheck();
		// add to the centre 
		this.add(constraintPanel, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Method to add an information panel to the baseCheckPanel dependent on the type of column
	 */
	public void addCheckInfo() {
		String typeInfo = "";
		if(colType.toString().equals("VarChar")) {
			typeInfo = "The program supports checks to ensure the length of the String satisfy the bounds given. Please enter an integer.";
		}
		else if(colType.toString().equals("Character")) {
			typeInfo = "The program supports checks to ensure that the character satisfies the bounds given. Please enter a single character from the alphabet.";
		}
		else if(colType.toString().equals("Date")) {
			typeInfo = "The program supports checks to ensure that the year is satisfied by the specified bounds. Please enter a year in the form YYYY.";
		}
		// add the string to the JPanel checkInfoPanel if the type is not an Integer
		if(!colType.toString().equals("Integer")) {
			JLabel info = new JLabel(typeInfo);
			checkInfoPanel.add(info);
			baseCheckPanel.add(checkInfoPanel);
		}
	}
	
	/**
	 * This method is entered when another check is to be added to the interface. It allows only 4
	 * checks to be added.
	 */
	public void addNewCheckToScreen() {
		// increment for the new check
		if(checkPos < MAX_CHECKS){
			addCheck();
		}
		else{
			JOptionPane.showMessageDialog(null, "Maximum number of checks have been reached", "Error Message", JOptionPane.ERROR_MESSAGE);
		}
		this.add(constraintPanel, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}

	/**
	 * This is entered when we want to add check options to the interface.
	 */
	public void addCheck() {
		// create a new panel and add the new check to it
		checkPanel.add(new JPanel());
		// add the position in the arraylists to the panel

		checkLabel.add(new JLabel(tableName+"."+colName));
		checkPanel.get(checkPos).add(checkLabel.get(checkPos));

		checkOpOptions.add(new JComboBox());
		// check the number of items in the last added checkOpOptions
		if(checkOpOptions.get(checkOpOptions.size()-1).getItemCount() == 0) {
			populateCheckCombo(checkOpOptions.get(checkPos));
		}
		checkPanel.get(checkPos).add(checkOpOptions.get(checkPos));

		checkVal.add(new JTextField(30));
		// set the default string if it is empty
		if(checkVal.get(checkPos).getText().equals("")) {
			checkVal.get(checkPos).setText("Enter Check Value");
		}
		checkVal.get(checkPos).addFocusListener(this);
		checkPanel.get(checkPos).add(checkVal.get(checkPos));

		deleteCheck.add(new JButton("Delete Check"));
		deleteCheck.get(checkPos).addActionListener(this);
		checkPanel.get(checkPos).add(deleteCheck.get(checkPos));

		// add the new panel to baseCheckPanel
		baseCheckPanel.add(checkPanel.get(checkPos));

		// add the 'add another check' button
		addNewCheck.setPreferredSize(new Dimension(5,5));
		baseCheckPanel.add(addNewCheck, BorderLayout.SOUTH);

		// add the baseCheckPanel to the constraintPanel
		constraintPanel.add(baseCheckPanel, BorderLayout.NORTH);

		checkPos++;
	}

	/**
	 * This method populates a combo box with options for check operators
	 * @param checkOption - JComboBox to be populated
	 */
	public void populateCheckCombo(JComboBox checkOption){
		checkOption.addItem("<");
		checkOption.addItem(">");
		checkOption.addItem(">=");
		checkOption.addItem("<=");
		checkOption.addItem("!=");
		if(!pk) {
			// if the column is the primary key then = is not a valid option
			checkOption.addItem("=");
		}
	}

	/**
	 * This method removes all check options from the interface. This will happen when the 
	 * check checkbox is deselected.
	 */
	public void removeCheckOptionsFromScreen() {
		// clear all arraylists related to check
		checkLabel.clear();
		checkOpOptions.clear();
		checkVal.clear();
		deleteCheck.clear();
		// remove the check info from the baseCheckPanel		
		checkPanel.removeAll(checkLabel);
		checkPanel.removeAll(checkOpOptions);
		checkPanel.removeAll(checkVal);
		checkPanel.removeAll(deleteCheck);

		for(int i = checkPanel.size()-1; i >= 0; i--) {
			baseCheckPanel.remove(checkPanel.get(i));
			checkPanel.remove(i);
		}	

		// remove the baseCheckPanel from the constraint panel
		constraintPanel.remove(baseCheckPanel);
		// reset the checkPos
		checkPos = 0;

		if(!defaultCheck.isSelected()){
			this.setSize(600,130);
		}

		// reset the check list
		db.getTable(tableName).getColumn(colName).setCheckList(new ArrayList<Check>());

		this.revalidate();
		this.repaint();
	}

	/**
	 * This deletes one specific check from the list of checks on the interface.
	 * @param index - the index of the specific check to remove
	 */
	public void deleteSpecificCheck(int index) {
		// delete the specific check from the list of checks in db
		ArrayList<Check> checks = new ArrayList<Check>();
		if(db.getTable(tableName).getColumn(colName).getCheckList() != null) {
			checks = db.getTable(tableName).getColumn(colName).getCheckList();
			if(checkOpOptions.get(index).getSelectedItem() != null && !checkVal.get(index).getText().trim().isEmpty()){
				for(int i = 0; i < checks.size(); i++) {
					if(checks.get(i).getCheckOperator().equals(checkOpOptions.get(index).getSelectedItem()) && 
							checks.get(i).getValue().equals(checkVal.get(index).getText().trim())){
						checks.remove(checks.get(i));
					}
				}
			}
			db.getTable(tableName).getColumn(colName).setCheckList(checks);
		}

		if(checks.size() == 0 && index == 0) {
			/* for this one we just want to clear the input as we dont want the option to
			 * add a check to disappear completely
			 */
			checkOpOptions.get(index).setSelectedIndex(0);
			checkVal.get(index).setText("");
		}
		else{
			// remove checkOpLabel, checkOpOptions, checkValLabel, checkVal, deleteCheck relating to index
			checkPanel.get(index).remove(checkLabel.get(index));
			checkLabel.remove(index);
			checkPanel.get(index).remove(checkOpOptions.get(index));
			checkOpOptions.get(index).removeAllItems();
			checkOpOptions.remove(index);
			checkPanel.get(index).remove(checkVal.get(index));
			checkVal.remove(index);
			checkPanel.get(index).remove(deleteCheck.get(index));
			deleteCheck.remove(index);

			// remove the panel also
			baseCheckPanel.remove(checkPanel.get(index));		
			checkPanel.remove(index);
			checkPos--;
		}

		constraintPanel.add(baseCheckPanel, BorderLayout.NORTH);

		this.revalidate();
		this.repaint();
	}

	/**
	 * This method adds the default functionality to the interface. This method is entered
	 * when the default checkbox is selected.
	 */
	public void addDefaultToScreen() {
		if(!check.isSelected()){
			this.setSize(700,250);
		}

		constraintPanel.add(defaultPanel, BorderLayout.SOUTH);
		this.add(constraintPanel, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}

	/**
	 * This method removes the default functionality from the interface. This is entered when the 
	 * default checkbox is deselected.
	 */
	public void removeDefaultFromScreen() {
		defaultField.setText("");
		constraintPanel.remove(defaultPanel);
		if(!check.isSelected()){
			this.setSize(600,130);
		}
		// remove default from the column
		db.getTable(tableName).getColumn(colName).setDefaultVal("");
		this.revalidate();
		this.repaint();
	}

	/**
	 * This is the ItemListener method, it listens for when there is a change of state in the
	 * check or default checkbox.
	 */
	public void itemStateChanged(ItemEvent e) {
		// find out if selected or deselected
		if(e.getStateChange() == ItemEvent.SELECTED){
			if(e.getItemSelectable() == check){
				this.addCheckSelected();
			}
			if(e.getItemSelectable() == defaultCheck) {
				this.addDefaultToScreen();
			}
		}
		else if(e.getStateChange() == ItemEvent.DESELECTED){
			if(e.getItemSelectable() == check) {
				this.removeCheckOptionsFromScreen();
			}
			if(e.getItemSelectable() == defaultCheck) {
				this.removeDefaultFromScreen();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == back){
			this.returnToPreviousPage();
		}
		else if(ae.getSource() == saveReturn) {
			// capture all info entered and go back to AddColumnInfo screen
			captureEnteredInformation();
		}
		else if(ae.getSource() == addNewCheck) { 
			this.addNewCheckToScreen();
		}
		for(int i = checkLabel.size()-1; i >= 0; i--){
			if(ae.getSource() == deleteCheck.get(i)){
				this.deleteSpecificCheck(i);
			}
		}
	}

	@Override
	public void focusGained(FocusEvent e) {	
		for(int i = 0; i < checkVal.size(); i++){
			if(e.getSource() == checkVal.get(i)) {
				if(checkVal.get(i).getText().trim().equals("Enter Check Value")){
					checkVal.get(i).setText("");
				}
			}
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		// don't do anything, if the user has entered something it should stay
	}
}

