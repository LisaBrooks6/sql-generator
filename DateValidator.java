import java.util.ArrayList;

public class DateValidator extends ConstraintValidator {
	ArrayList<String> solution;
	ArrayList<Integer> yearFormat = new ArrayList<Integer>();
	
	public DateValidator(ArrayList<String> solution, ArrayList<Check> checkList, boolean notNull, boolean unique, int noData) {
		this.solution = solution;
		this.checkList = checkList;
		this.notNull = notNull;
		this.unique = unique;
		this.noData = noData;
	}
	
	@Override
	protected boolean validate() {
		boolean pass = true;
		if(solution.size()!=noData) {
			pass = false;
			System.out.println("Date failed as solution did not match the number of data required");
		}
		if(notNull) {
			for(String s: solution) {
				if(s.equals("null")) {
					System.out.println("Date failed as null value was found");
					pass = false;
				}
			}
		}
		if(unique) {
			ArrayList<String> uniqueValidation = new ArrayList<String>();
			for(String s: solution) {
				if(!uniqueValidation.contains(s)) {
					uniqueValidation.add(s);
				}
				else{
					// this already exists in the arraylist, hence not unique
					System.out.println("Date failed as duplicate was found");
					pass = false;
				}
			}
		}
		if(checkList!=null) {
			this.changeToYear();
			for(Check c: checkList) {
				String op = c.getCheckOperator();
				// the val will always be an Integer due to the input constraints
				Integer val = Integer.parseInt(c.getValue());
				if(checkValidator(op, val) == false) {
					pass = false;
					System.out.println("Date failed on check: "+op+" "+val);
					// else pass will be true and therefore nothing needs to be done
				}
			}
		}
		
		return pass;
	}

	public void changeToYear() {
		for(String s: solution) {
			// the string will come in as day/month/year
			String[] dateParts = s.split("[ /]+");
			// day and month will be the first two positions in the array, year will be at pos 2
			Integer year = Integer.parseInt(dateParts[2]);
			yearFormat.add(year);
		}	
	}
	
	public boolean checkValidator(String op, Integer val) {
		if(op.equals("<")) {
			for(Integer i: yearFormat) {
				if(!(i < val)){
					return false;
				}
			}
		}
		else if(op.equals(">")) {
			for(Integer i: yearFormat) {
				if(!(i > val)){
					return false;
				}
			}
		}
		else if(op.equals("=")) {
			for(Integer i: yearFormat) {
				if(i != val){
					return false;
				}
			}
		}
		else if(op.equals("!=")) {
			for(Integer i: yearFormat) {
				if(i == val){
					return false;
				}
			}
		}
		else if(op.equals("<=")) {
			for(Integer i: yearFormat) {
				if(i > val){
					return false;
				}
			}
		}
		else if(op.equals(">=")) {
			for(Integer i: yearFormat) {
				if(i < val){
					return false;
				}
			}
		}
		return true;
	}
}