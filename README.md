# README #

### What is this repository for? ###

* This repository is a SQL Generator to allow users to automate the creation of database scripts (create and population scripts) from entering a schema of their choice. Subsequently the SQL Generator will allow the user to create sample SQL statements relating to their entered schema.
* The file UML_diagram.png will give an overview of the program's classes and their relationships.
* Version 1.0

### How do I get set up? ###

* Clone the repository
* Run the program's jar - sqlGenerator.jar. This can be run by entering the command 'java -jar sqlGenerator.jar' into terminal.

### Example usage ###

* Create a database called University with 3 tables
* First table is Student with 3 columns - Name (Varchar of length 40), ID (Integer, primary key), advisor (Integer)
* Second table is Course with 3 columns - Name (Varchar of length 50), ID (Integer, primary key), lecturer (Integer)
* Third table is Lecturer with 2 columns - Name (Varchar of length 40), ID (Integer, primary key)
* The foreign keys are: Student.advisor = Lecturer.ID; Course.lecturer = Lecturer.ID
* Generate the create script
* Generate the population script choosing relevant data files for each column
* Generate sample SQL statements

### View a video online of a sample usage ###

* This video will show an example of making extensive use of the constraint programming used in the development of the SQL Generator - https://www.youtube.com/watch?v=ZcbDIsXxWsM

### Who do I talk to? ###

* Lisa Brooks (brookslisa224@gmail.com)