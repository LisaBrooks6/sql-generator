import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import org.chocosolver.solver.*;
import org.chocosolver.solver.variables.*;
import org.chocosolver.solver.constraints.*;
import org.chocosolver.solver.search.strategy.*;
import org.chocosolver.solver.exception.ContradictionException;

/*
 * The aim of this class is to deal with constraint on columns of type
 * VarChar. The class will deal with not null, checks and unique.
 */

public class VarCharSolver extends ConstraintSolver {
	private ArrayList<String> fileContent = new ArrayList<String>(); 
	private Map<Integer, ArrayList<String>> correspondingStrings = new HashMap<Integer, ArrayList<String>>();

	/**
	 * Constructor for VarCharSolver
	 * @param fileName
	 * @param checkList
	 * @param unique
	 * @param notNull
	 * @param type
	 * @param noData
	 */
	public VarCharSolver(String fileName, ArrayList<Check> checkList, boolean unique, boolean notNull, String type, int noData) {
		this.fileName = fileName;
		this.checkList = checkList;
		this.type = type;
		this.noData = noData;
		this.unique = unique;
		this.notNull = notNull;
		
		solver = new Solver("varchar_solver");
		this.initialSetUp();
	}

	/**
	 * Method to set up the IntVar array.
	 */
	public void initialSetUp() {

		this.readFile();
		// move the lengths of the strings in fileContent into an int array 
		int [] content = new int[fileContent.size()];
		int i = 0;
		for(String data : fileContent) {
			// load the array position with the file content
			content[i] = data.toString().trim().length();
			ArrayList<String> vals;
			if(correspondingStrings.containsKey(content[i])) {
				vals = correspondingStrings.get(content[i]);
				vals.add(data.toString().trim());
			}
			else{ 
				// the key is not in the map
				vals = new ArrayList<String>();
				vals.add(data.toString().trim());
			}
			correspondingStrings.put(content[i], vals);	
			// increment to the next position
			i++;
		}

		/* 
		 * the data array is of length noData. Each value 
		 * has domain of all values in content.
		 */
		data = VF.enumeratedArray("data", noData, content, solver);

		try {
			this.addConstraints();
		} 
		catch (ContradictionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to add the constraints that the data IntVar array must satisfy.
	 */
	public void addConstraints() throws ContradictionException {
		// check constraint
		for(int i = 0; i < data.length; i++) {
			if(checkList != null) {
				for(Check c : checkList) {
					int val = Integer.parseInt(c.getValue().trim());
					// post a constraint to check the length of the string is valid
					solver.post(ICF.arithm(data[i], c.getCheckOperator().trim(), val));
				}
			}
		}
		// heuristics to ensure the data is a random value each time
		solver.set(ISF.random_value(data));
		// propagate
		try {
			solver.propagate();
		}
		catch(ContradictionException e) {
			// if it ends up in here then I want to end the loop 
			throw new ContradictionException();
		}
	}

	/**
	 * Method to read the input file.
	 */
	public void readFile() {
		// loop through the file and find the min and max values
		try {
			FileReader reader = new FileReader(fileName);
			Scanner sc = new Scanner(reader);
			while(sc.hasNextLine()) {
				String content = sc.nextLine();
				// remove all whitespace from content
				content = content.replaceAll("\\s+", "");
				if(notNull) {
					/* the column has a not null constraint therefore do not add any 
					 * null values to the fileContent arraylist.
					 */
					if(!content.equals("null")) {
						fileContent.add(content);
					}
				}
				else {
					fileContent.add(content);					
				}
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("The file "+fileName+" was not found");
		}
	}

	/**
	 * Method to return the outcome of whether a solution was found or not
	 * @return true/false 
	 */
	public boolean solve() {
		return solver.findSolution();
	}
	
	/**
	 * Method to return the solution which satisfies the constraints
	 * @return data
	 */
	public IntVar[] getData() {
		return data;
	}

	/**
	 * Method to return an arraylist of corresponding strings of the solution.
	 * @return stringData
	 */
	public ArrayList<String> getStringData() {
		/* due to the use of a map in this constraint handler, we cannot use the alldifferent constraint to 
		 * deal with unique.
		 */
		ArrayList<String> stringData = new ArrayList<String>();
		while(stringData.size() < data.length){
			for(IntVar s : data) {
				ArrayList<String> vals = correspondingStrings.get(s.getValue());
				Random r = new Random();
				int randVal = r.nextInt(vals.size());
				if(unique) {
					if(!stringData.contains(vals.get(randVal).trim())) {
						// the string is not already in the arrayList therefore it can be added
						stringData.add(vals.get(randVal).trim());
					}
				}
				else {
					stringData.add(vals.get(randVal).trim());
				}
			}
		}
		return stringData;
	}
}
