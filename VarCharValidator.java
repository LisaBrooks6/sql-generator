import java.util.ArrayList;

public class VarCharValidator extends ConstraintValidator {

	private ArrayList<String> solution;
	
	public VarCharValidator(ArrayList<String> solution, ArrayList<Check> checkList, boolean notNull, boolean unique, int noData) {
		this.solution = solution;
		this.checkList = checkList;
		this.notNull = notNull;
		this.unique = unique;
		this.noData = noData;
	}
	
	@Override
	protected boolean validate() {
		boolean pass = true;
		if(solution.size()!=noData) {
			System.out.println("VarChar failed as solution is not equal to noData");
			pass = false;
		}
		if(checkList!=null) {
			for(Check c: checkList) {
				String op = c.getCheckOperator();
				// the val will always be an Integer due to the input constraints
				Integer val = Integer.parseInt(c.getValue());
				if(checkValidator(op, val) == false) {
					pass = false;
					System.out.println("VarChar failed for check : "+op+" "+val);
					// else pass will be true and therefore nothing needs to be done
				}
			}
		}
		if(notNull) {
			for(String s: solution) {
				if(s.equals("null")) {
					System.out.println("VarChar failed due to a null value");
					pass = false;
				}
			}
		}
		if(unique) {
			ArrayList<String> uniqueValidation = new ArrayList<String>();
			for(String s: solution) {
				if(!uniqueValidation.contains(s)) {
					uniqueValidation.add(s);
				}
				else{
					// this already exists in the arraylist, hence not unique
					pass = false;
					System.out.println("VarChar failed as duplicate was found");
				}
			}
		}
		return pass;
	}

	public boolean checkValidator(String op, Integer val) {
		if(op.equals("<")) {
			for(String s: solution) {
				if(!(s.length() < val)){
					return false;
				}
			}
		}
		else if(op.equals(">")) {
			for(String s: solution) {
				if(!(s.length() > val)){
					return false;
				}
			}
		}
		else if(op.equals("=")) {
			for(String s: solution) {
				if(s.length() != val){
					return false;
				}
			}
		}
		else if(op.equals("!=")) {
			for(String s: solution) {
				if(s.length() == val){
					return false;
				}
			}
		}
		else if(op.equals("<=")) {
			for(String s: solution) {
				if(s.length() > val){
					return false;
				}
			}
		}
		else if(op.equals(">=")) {
			for(String s: solution) {
				if(s.length() < val){
					return false;
				}
			}
		}
		return true;
	}
}
