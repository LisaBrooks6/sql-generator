
public class ChosenSqlKeywords {
	private boolean distinct, where, and, between, or, in, equals, groupBy, orderBy, asc, desc;
	private boolean count, sum, avg, first, last, max, min, ucase, lcase;   // functions
	private String joinType;
	
	/**
	 * Constructor for ChosenSqlKeywords
	 * @param distinct
	 * @param where
	 * @param and
	 * @param between
	 * @param or
	 * @param in
	 * @param equals
	 * @param groupby
	 * @param orderby
	 * @param asc
	 * @param desc
	 * @param joinType
	 * @param count
	 * @param sum
	 * @param avg
	 * @param first
	 * @param last
	 * @param max
	 * @param min
	 * @param ucase
	 * @param lcase
	 */
	public ChosenSqlKeywords(boolean distinct, boolean where, boolean and, boolean or, boolean between, boolean in, boolean equals, boolean groupby, boolean orderby, 
			boolean asc, boolean desc, String joinType, boolean count, boolean sum, boolean avg, boolean first, boolean last, 
			boolean max, boolean min, boolean ucase, boolean lcase) {
		this.distinct = distinct;
		this.where = where;
		this.and = and;
		this.or = or;
		this.between = between;
		this.in = in;
		this.equals = equals;
		this.groupBy = groupby;
		this.orderBy = orderby;
		this.asc = asc;
		this.desc = desc;
		this.joinType = joinType;
		
		this.count = count;
		this.sum = sum;
		this.avg = avg;
		this.first = first;
		this.last = last;
		this.max = max;
		this.min = min;
		this.ucase = ucase;
		this.lcase = lcase;
	}

	/**
	 * Accessor for distinct
	 * @return distinct
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * Mutator for distinct
	 * @param distinct
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}
	
	/**
	 * Accessor for where
	 * @return where
	 */
	public boolean isWhere() {
		return where;
	}

	/**
	 * Mutator for where
	 * @param where
	 */
	public void setWhere(boolean where) {
		this.where = where;
	}

	/**
	 * Accessor for and
	 * @return and
	 */
	public boolean isAnd() {
		return and;
	}

	/**
	 * Mutator for and
	 * @param and
	 */
	public void setAnd(boolean and) {
		this.and = and;
	}

	/**
	 * Accessor for between
	 * @return between
	 */
	public boolean isBetween() {
		return between;
	}

	/**
	 * Mutator for between
	 * @param between
	 */
	public void setBetween(boolean between) {
		this.between = between;
	}
	
	/**
	 * Accessor for equals
	 * @return equals
	 */
	public boolean isEquals() {
		return equals;
	}

	/**
	 * Mutator for equals
	 * @param equals
	 */
	public void setEquals(boolean equals) {
		this.equals = equals;
	}
	
	/**
	 * Accessor for or 
	 * @return or
	 */
	public boolean isOr() {
		return or;
	}

	/**
	 * Mutator for or
	 * @param or
	 */
	public void setOr(boolean or) {
		this.or = or;
	}

	/**
	 * Accessor for in
	 * @return in
	 */
	public boolean isIn() {
		return in;
	}

	/**
	 * Mutator for in
	 * @param in
	 */
	public void setIn(boolean in) {
		this.in = in;
	}
	
	/**
	 * Accessor for groupBy
	 * @return groupBy
	 */
	public boolean isGroupBy() {
		return groupBy;
	}

	/**
	 * Mutator for groupBy
	 * @param groupBy
	 */
	public void setGroupBy(boolean groupBy) {
		this.groupBy = groupBy;
	}

	/**
	 * Accessor for orderBy
	 * @return orderBy
	 */
	public boolean isOrderBy() {
		return orderBy;
	}

	/**
	 * Mutator for orderBy
	 * @param orderBy
	 */
	public void setOrderBy(boolean orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * Accessor for asc
	 * @return asc
	 */
	public boolean isAsc() {
		return asc;
	}

	/**
	 * Mutator for asc
	 * @param asc
	 */
	public void setAsc(boolean asc) {
		this.asc = asc;
	}
	
	/**
	 * Accessor for desc
	 * @return desc
	 */
	public boolean isDesc() {
		return desc;
	}

	/**
	 * Mutator for desc
	 * @param desc
	 */
	public void setDesc(boolean desc) {
		this.desc = desc;
	}

	/**
	 * Accessor for joinType
	 * @return joinType
	 */
	public String getJoinType() {
		return joinType;
	}

	/**
	 * Mutator for joinType
	 * @param joinType
	 */
	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	/**
	 * Accessor for count
	 * @return count
	 */
	public boolean isCount() {
		return count;
	}

	/**
	 * Mutator for count
	 * @param count
	 */
	public void setCount(boolean count) {
		this.count = count;
	}

	/**
	 * Accessor for sum
	 * @return sum
	 */
	public boolean isSum() {
		return sum;
	}

	/**
	 * Mutator for sum
	 * @param sum
	 */
	public void setSum(boolean sum) {
		this.sum = sum;
	}

	/**
	 * Accessor for avg
	 * @return avg
	 */
	public boolean isAvg() {
		return avg;
	}

	/**
	 * Mutator for avg
	 * @param avg
	 */
	public void setAvg(boolean avg) {
		this.avg = avg;
	}

	/**
	 * Accessor for first
	 * @return first
	 */ 
	public boolean isFirst() {
		return first;
	}

	/**
	 * Mutator for first
	 * @param first
	 */
	public void setFirst(boolean first) {
		this.first = first;
	}

	/**
	 * Accessor for last
	 * @return last
	 */
	public boolean isLast() {
		return last;
	}

	/**
	 * Mutator for last
	 * @param last
	 */
	public void setLast(boolean last) {
		this.last = last;
	}

	/**
	 * Accessor for max
	 * @return max
	 */ 
	public boolean isMax() {
		return max;
	}

	/**
	 * Mutator for max
	 * @param max
	 */
	public void setMax(boolean max) {
		this.max = max;
	}

	/**
	 * Accessor for min
	 * @return min
	 */
	public boolean isMin() {
		return min;
	}

	/**
	 * Mutator for min
	 * @param min
	 */
	public void setMin(boolean min) {
		this.min = min;
	}

	/**
	 * Accessor for ucase
	 * @return ucase
	 */
	public boolean isUcase() {
		return ucase;
	}

	/**
	 * Mutator for ucase
	 * @param ucase
	 */
	public void setUcase(boolean ucase) {
		this.ucase = ucase;
	}

	/**
	 * Accessor for lcase
	 * @return lcase
	 */
	public boolean isLcase() {
		return lcase;
	}

	/**
	 * Mutator for lcase
	 * @param lcase
	 */
	public void setLcase(boolean lcase) {
		this.lcase = lcase;
	}
}
